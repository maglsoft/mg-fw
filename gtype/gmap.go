package gtype

import (
	"fmt"
	"github.com/mitchellh/mapstructure"
	"github.com/spf13/cast"
)

type GMap map[string]any

func (p GMap) GetAny(key string) interface{} {
	if v, ok := p[key]; ok {
		return v
	}
	return nil
}

func (p GMap) GetString(key string) string {
	if v := p.GetAny(key); v != nil {
		if s, ok := v.(string); ok {
			return s
		} else if stringer, ok := v.(fmt.Stringer); ok {
			return stringer.String()
		}
	}
	return ""
}

func (p GMap) GetInt(key string, def int) int {
	if v := p.GetAny(key); v != nil {
		switch val := v.(type) {
		case int8:
			return int(val)
		case uint8:
			return int(val)
		case int16:
			return int(val)
		case uint16:
			return int(val)
		case int32:
			return int(val)
		case uint32:
			return int(val)
		case int64:
			return int(val)
		case uint64:
			return int(val)
		case float32:
			return int(val)
		case float64:
			return int(val)
		case bool:
			if val {
				return 1
			}
			return 0
		default:
			return cast.ToInt(v)
		}
	}
	return def
}

func (p GMap) GetFloat32(key string, def float32) float32 {
	if v := p.GetAny(key); v != nil {
		switch val := v.(type) {
		case float32:
			return val
		case float64:
			return float32(val)
		case int8:
			return float32(val)
		case uint8:
			return float32(val)
		case int32:
			return float32(val)
		case uint32:
			return float32(val)
		case int64:
			return float32(val)
		case uint64:
			return float32(val)
		default:
			return cast.ToFloat32(v)
		}
	}
	return def
}

func (p GMap) GetFloat64(key string, def float64) float64 {
	if v := p.GetAny(key); v != nil {
		switch val := v.(type) {
		case float64:
			return val
		case float32:
			return float64(val)
		case int8:
			return float64(val)
		case uint8:
			return float64(val)
		case int32:
			return float64(val)
		case uint32:
			return float64(val)
		case int64:
			return float64(val)
		case uint64:
			return float64(val)
		default:
			return cast.ToFloat64(v)
		}
	}
	return def
}

func (p GMap) GetStrMap(key string) map[string]any {
	if v := p.GetAny(key); v != nil {
		if m, ok := v.(map[string]any); ok {
			return m
		}
	}
	return nil
}

func (p GMap) GetSliceStrMap(key string) []map[string]any {
	if v := p.GetAny(key); v != nil {
		if sl, ok := v.([]map[string]any); ok {
			return sl
		}
	}
	return nil
}

func (p GMap) BindTo(data any) error {
	decode, err := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
		ErrorUnused:      false,
		ZeroFields:       false, // 合并处理
		WeaklyTypedInput: true,  // 自动转换
		Metadata:         nil,   // 不收集metadata
		Result:           data,
		TagName:          "json",
	})
	if err != nil {
		return err
	}
	return decode.Decode(p)
}
