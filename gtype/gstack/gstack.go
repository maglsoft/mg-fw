package gstack

import "sync"

type GStack[T any] struct {
	items  []T
	lock   sync.RWMutex
	unsafe bool
}

// New 创建栈
func New[T any](unsafe ...bool) *GStack[T] {
	result := &GStack[T]{
		items:  make([]T, 0, 10),
		unsafe: false,
	}
	if len(unsafe) > 0 {
		result.unsafe = unsafe[0]
	}
	return result
}

// Push 入栈
func (s *GStack[T]) Push(item T) {
	if s.unsafe {
		s.items = append([]T(s.items), item)
		return
	}
	s.lock.Lock()
	defer s.lock.Unlock()
	s.items = append([]T(s.items), item)
}

// Pop 出栈，为空时返回nil
func (s *GStack[T]) Pop() *T {
	if s.IsEmpty() {
		return nil
	}
	if s.unsafe {
		item := s.items[len([]T(s.items))-1]
		s.items = s.items[:len([]T(s.items))-1]
		return &item
	}
	s.lock.Lock()
	defer s.lock.Unlock()
	item := s.items[len([]T(s.items))-1]
	s.items = s.items[:len([]T(s.items))-1]
	return &item
}

// First 获取第一个进入队列的项
func (s *GStack[T]) First() *T {
	if s.IsEmpty() {
		return nil
	}
	if s.unsafe {
		item := s.items[0]
		return &item
	}
	s.lock.Lock()
	defer s.lock.Unlock()
	item := s.items[0]
	return &item
}

// Last 获取最后进入队列的项
func (s GStack[T]) Last() *T {
	if s.IsEmpty() {
		return nil
	}
	if s.unsafe {
		item := s.items[len([]T(s.items))-1]
		return &item
	}
	s.lock.Lock()
	defer s.lock.Unlock()
	item := s.items[len([]T(s.items))-1]
	return &item
}

// IsEmpty 检查是否为空
func (s GStack[T]) IsEmpty() bool {
	return len([]T(s.items)) == 0
}

// Size 堆栈大小
func (s GStack[T]) Size() int {
	return len([]T(s.items))
}

// Clear 清空
func (s GStack[T]) Clear() {
	if s.unsafe {
		s.items = make([]T, 0, 10)
		return
	}
	s.lock.Lock()
	defer s.lock.Unlock()
	s.items = make([]T, 0, 10)
	return
}
