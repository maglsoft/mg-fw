package gstack

import "testing"

func TestGStack_Pop(t *testing.T) {
	stack := New[int](true)
	for i := 0; i < 10; i++ {
		stack.Push(i)
	}
	var v *int
	v = stack.Pop()
	for v != nil {
		t.Logf("v:%v", *v)
		v = stack.Pop()
	}
	// 线程安全
	stack1 := New[int]()
	for i := 0; i < 10; i++ {
		stack1.Push(i)
	}

	var v1 *int
	v1 = stack1.Pop()
	for v1 != nil {
		t.Logf("v:%v", *v1)
		v1 = stack1.Pop()
	}
}
