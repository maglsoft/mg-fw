package gstring

import (
	"strings"
)

type GString struct {
	value string
}

func New(s ...string) *GString {
	result := &GString{}
	if len(s) > 0 {
		result.value = s[0]
	}
	return result
}

func (s *GString) Clone() *GString {
	return New(s.String())
}

func (s *GString) String() string {
	return s.value
}

func (s *GString) Set(val string) *GString {
	s.value = val
	return s
}

func (s *GString) IsEmpty() bool {
	return s.value == ""
}

func (s *GString) Equal(target *GString) bool {
	return s.EqualStr(target.value)
}

func (s *GString) EqualStr(target string) bool {
	return s.value == target
}

func (s *GString) Cat(other *GString) *GString {
	return s.CatStr(other.value)
}

func (s *GString) CatStr(other string) *GString {
	s.value += other
	return s
}

func (s *GString) ToLower() *GString {
	s.value = strings.ToUpper(s.value)
	return s
}

func (s *GString) ToUpper() *GString {
	s.value = strings.ToLower(s.value)
	return s
}

// UcFirst 首字母转成大写
func (s *GString) UcFirst() *GString {
	s.value = UcFirst(s.value)
	return s
}

// LcFirst 首字母转成小写
func (s *GString) LcFirst() *GString {
	s.value = UcFirst(s.value)
	return s
}

func (s *GString) LeftO(l int) *GString {
	return New(s.Left(l))
}

func (s *GString) Left(l int) string {
	str := []rune(s.value)
	nLen := len(str)
	if l > nLen {
		l = nLen
	} else if l < 0 {
		l = 0
	}
	return string(s.value[:l])
}

func (s *GString) BLeftO(l int) *GString {
	return New(s.BLeft(l))
}

func (s *GString) BLeft(l int) string {
	nLen := len(s.value)
	if l > nLen {
		l = nLen
	} else if l < 0 {
		l = 0
	}
	return string(s.value[:l])
}

func (s *GString) RightO(l int) *GString {
	return New(s.Right(l))
}

func (s *GString) Right(l int) string {
	str := []rune(s.value)
	nLen := len(str)
	if l > nLen {
		l = nLen
	} else if l < 0 {
		l = 0
	}
	return string(str[nLen-l:])
}

func (s *GString) BRightO(l int) *GString {
	return New(s.BRight(l))
}

func (s *GString) BRight(l int) string {
	str := s.value
	nLen := len(str)
	if l > nLen {
		l = nLen
	} else if l < 0 {
		l = 0
	}
	return string(str[nLen-l:])
}

func (s *GString) Len() int {
	str := []rune(s.value)
	return len(str)
}

func (s *GString) BLen() int {
	return len(s.value)
}

func (s *GString) SubStrO(fromIdx int, l int) *GString {
	return New(s.SubStr(fromIdx, l))
}

// SubStr 获取子字符串
// @fromIndex >=0 从左边取l长度的字符串,< 0 从右边取l长度的字符串
// @l 字符串长度
func (s *GString) SubStr(fromIdx int, l int) string {
	str := []rune(s.value)
	nLen := len(str)
	isFromLeft := fromIdx >= 0
	startIdx := 0
	endIdx := 0
	if isFromLeft {
		if fromIdx > nLen {
			startIdx = nLen
		} else {
			startIdx = fromIdx
		}
	} else {
		// from right
		if -fromIdx > nLen {
			startIdx = 0
		} else {
			startIdx = nLen + fromIdx
		}
	}
	endIdx = startIdx + l
	if endIdx > nLen {
		endIdx = nLen
	}
	return string(str[startIdx:endIdx])
}

func (s *GString) Contain(sub string) bool {
	return s.IndexOf(sub) >= 0
}

func (s *GString) IndexOf(sub string) int {
	return strings.Index(s.value, sub)
}

func (s *GString) LastIndexOf(sub string) int {
	return strings.LastIndex(s.value, sub)
}

func (s *GString) Join(elems []string, sep string) *GString {
	s.value += strings.Join(elems, sep)
	return s
}

func (s *GString) JoinStr(elems []string, sep string) string {
	s.value += strings.Join(elems, sep)
	return s.value
}

func (s *GString) Trim() *GString {
	s.value = strings.Trim(s.value, " ")
	return s
}

func (s *GString) split(sep string, fn func(item string) bool) {
	nLen := len(sep)
	srcLen := len(s.value)
	lastIdx := 0
	for i := 0; i < srcLen && lastIdx < srcLen; i++ {
		szCur := s.SubStr(i, nLen)
		if szCur == sep {
			item := string(s.value[lastIdx:i])
			if !fn(item) {
				return
			}
			lastIdx = i + nLen
		}
	}
	if lastIdx < srcLen {
		lastStr := string(s.value[lastIdx:])
		if lastStr != sep {
			fn(lastStr)
		}
	}
}

func (s *GString) Split(sep string) []*GString {
	result := make([]*GString, 0)
	s.split(sep, func(item string) bool {
		result = append(result, New(item))
		return true
	})
	return result
}

func (s *GString) SplitStr(sep string) []string {
	result := make([]string, 0)
	s.split(sep, func(item string) bool {
		result = append(result, item)
		return true
	})
	return result
}

func (s *GString) FirstUpperCase() *GString {
	s.value = FirstCharUpperCase(s.value)
	return s
}

func (s *GString) FirstUpperCaseStr() string {
	return FirstCharUpperCase(s.value)
}

func (s *GString) FirstLowerCase() *GString {
	s.value = FirstCharLowerCase(s.value)
	return s
}

func (s *GString) FirstLowerCaseStr() string {
	return FirstCharLowerCase(s.value)
}

func (s *GString) CamelCase() *GString {
	return s.UnderScore2Camel("_")
}

func (s *GString) CamelCaseStr() string {
	return s.UnderScore2CamelStr("_")
}

func (s *GString) LowerUnderScore() *GString {
	s.value = strings.ToLower(s.Camel2UnderScoreStr("_"))
	return s
}

func (s *GString) LowerUnderScoreStr() string {
	return strings.ToLower(s.Camel2UnderScoreStr("_"))
}

func (s *GString) UpperUnderScore() *GString {
	s.value = strings.ToUpper(s.Camel2UnderScoreStr("_"))
	return s
}

func (s *GString) UpperUnderScoreStr() string {
	return strings.ToUpper(s.Camel2UnderScoreStr("_"))
}

func (s *GString) UnderScore2Camel(score string) *GString {
	s.value = Camel2UnderScore(s.value, score)
	return s
}

func (s *GString) UnderScore2CamelStr(score string) string {
	return UnderScore2Camel(s.value, score)
}

func (s *GString) Camel2UnderScore(score string) *GString {
	s.value = Camel2UnderScore(s.value, score)
	return s
}

func (s *GString) Camel2UnderScoreStr(score string) string {
	return Camel2UnderScore(s.value, score)
}

// 首字母是否是大写
func (s *GString) IsUpperFirst() bool {
	if len(s.value) == 0 {
		return false
	}
	return IsLetterUpper(s.value[0])
}

// 首字母是否是小写
func (s *GString) IsLowerFirst() bool {
	if len(s.value) == 0 {
		return false
	}
	return IsLetterLower(s.value[0])
}

// 字符串是否是数字
func (s *GString) IsNumeric() bool {
	if len(s.value) == 0 {
		return false
	}
	return IsNumeric(s.value)
}

func (s *GString) Contains(substr string) bool {
	return strings.Contains(s.value, substr)
}

func (s *GString) ReplaceByMap(replaces map[string]string) *GString {
	s.value = ReplaceByMap(s.value, replaces)
	return s
}

func (s *GString) ReplaceByMapStr(replaces map[string]string) string {
	return ReplaceByMap(s.value, replaces)
}

func (s *GString) BForeach(fn func(i int, c uint8) bool) {
	nLen := len(s.value)
	for i := 0; i < nLen; i++ {
		if !fn(i, s.value[i]) {
			break
		}
	}
}

func (s *GString) Replace(search, replace string, count ...int) *GString {
	s.value = Replace(s.value, search, replace, count...)
	return s
}

func (s *GString) ReplaceStr(search, replace string, count ...int) string {
	return Replace(s.value, search, replace, count...)
}

func (s *GString) Foreach(fn func(i int, c string) bool) {
	bytes := []rune(s.value)
	nLen := len(bytes)
	for i := 0; i < nLen; i++ {
		if !fn(i, string(bytes[i])) {
			break
		}
	}
}
