package gqueue

import "testing"

func TestGQueue_Front(t *testing.T) {
	queue := New[int]()
	for i := 0; i < 10; i++ {
		queue.Offer(i)
	}
	for v := queue.Peek(); v != nil; v = queue.Peek() {
		t.Logf("v:%d", *v)
	}
}
