package gqueue

import "sync"

type GQueue[T any] struct {
	items  []T
	lock   sync.RWMutex
	unsafe bool
}

// New 创建FIFO队列
func New[T any](unsafe ...bool) *GQueue[T] {
	result := &GQueue[T]{
		items:  make([]T, 0, 10),
		unsafe: false,
	}
	if len(unsafe) > 0 {
		result.unsafe = unsafe[0]
	}
	return result
}

// Offer 入队列
func (q *GQueue[T]) Offer(item T) {
	if q.unsafe {
		q.items = append([]T(q.items), item)
		return
	}
	q.lock.Lock()
	defer q.lock.Unlock()
	q.items = append([]T(q.items), item)
}

// Peek 出队列
func (q *GQueue[T]) Peek() *T {
	if q.IsEmpty() {
		return nil
	}
	if q.unsafe {
		item := q.items[0]
		q.items = q.items[1:len([]T(q.items))]
		return &item
	}
	q.lock.Lock()
	defer q.lock.Unlock()
	item := q.items[0]
	q.items = q.items[1:len([]T(q.items))]
	return &item
}

// Front 获取队列的第一个元素，不移除
func (q *GQueue[T]) Front() *T {
	if q.IsEmpty() {
		return nil
	}
	if q.unsafe {
		item := q.items[0]
		return &item
	}
	q.lock.Lock()
	defer q.lock.Unlock()
	item := q.items[0]
	return &item
}

// Last 取队列最后一个元素，不移除
func (q *GQueue[T]) Last() *T {
	if q.IsEmpty() {
		return nil
	}
	if q.unsafe {
		item := q.items[len([]T(q.items))-1]
		return &item
	}
	q.lock.Lock()
	defer q.lock.Unlock()
	item := q.items[len([]T(q.items))-1]
	return &item
}

// IsEmpty 判断是否为空
func (q *GQueue[T]) IsEmpty() bool {
	return len([]T(q.items)) == 0
}

// Size 获取队列的长度
func (q *GQueue[T]) Size() int {
	return len([]T(q.items))
}
