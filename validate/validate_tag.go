package validate

import (
	"github.com/go-playground/locales/zh"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	zh_translations "github.com/go-playground/validator/v10/translations/zh"
)

var _validator *validator.Validate
var trans ut.Translator

var validateErrTagName = "err_msg"
var validateTagName = "rule"

// SetRuleTag 设置数据验证规则的tag名称
func SetRuleTag(tagName string) {
	validateTagName = tagName
}

// SetErrMsgTag 设置错误标签名称
func SetErrMsgTag(tagName string) {
	validateErrTagName = tagName
}

func init() {
	// 中文翻译器
	uni := ut.New(zh.New())
	trans, _ = uni.GetTranslator("zh")
	_validator = validator.New()
	// 注册翻译器到校验器
	err := zh_translations.RegisterDefaultTranslations(_validator, trans)
	if err != nil {
		panic(err)
		//fmt.Println(err)
	}
}
