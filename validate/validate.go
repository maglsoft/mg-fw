package validate

import (
	"errors"
)

// ValidStruct 验证数据的有效性
func ValidStruct(data any) error {
	if data == nil {
		return errors.New("验证的数据为空")
	}
	_validator.SetTagName(validateTagName)
	err := _validator.Struct(data)
	return translateError(data, err)
}

func ValidStructWithTag(data any, tagName string) error {
	if data == nil {
		return errors.New("验证的数据为空")
	}
	_validator.SetTagName(tagName)
	err := _validator.Struct(data)
	return translateError(data, err)
}

func ValidVar(data any, tag string) error {
	if data == nil {
		return errors.New("验证的数据为空")
	}
	_validator.SetTagName(validateTagName)
	err := _validator.Var(data, tag)
	return translateError(data, err)
}
