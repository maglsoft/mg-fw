package validate

import (
	"encoding/json"
	"github.com/go-playground/validator/v10"
	"reflect"
)

type ValidErrors struct {
	errors []error
}

func NewValidErrors(errs ...error) *ValidErrors {
	result := &ValidErrors{
		errors: make([]error, 0, 1),
	}
	if errs != nil && len(errs) > 0 {
		result.errors = append(result.errors, errs...)
	}
	return result
}
func (ve *ValidErrors) IsError() bool {
	return ve.errors != nil && len(ve.errors) > 0
}

func (ve *ValidErrors) Error() string {
	if ve.IsError() {
		return ve.errors[0].Error()
	}
	return ""
}

func (ve *ValidErrors) Add(err error) {
	if err != nil {
		ve.errors = append(ve.errors, err)
	}
}
func (ve *ValidErrors) Clear() {
	ve.errors = make([]error, 0)
}

func (ve *ValidErrors) Errors() []error {
	return ve.errors
}

func (ve *ValidErrors) MarshalJSON() ([]byte, error) {
	return json.Marshal(ve.errors)
}

type ValidFieldError struct {
	Field      string `json:"field"`
	FieldError string `json:"error"`
}

func (vf *ValidFieldError) Error() string {
	return vf.FieldError
}

func NewFieldError(fieldName string, error string) *ValidFieldError {
	return &ValidFieldError{
		Field:      fieldName,
		FieldError: error,
	}
}

func translateError(data any, err error) error {
	if err != nil {
		validationErrs, ok := err.(validator.ValidationErrors)
		if ok {
			var uType = reflect.TypeOf(data)
			for uType != nil && uType.Kind() == reflect.Ptr {
				uType = uType.Elem()
			}
			errs := NewValidErrors()
			for _, err := range validationErrs {
				fieldName := err.Field()                  //获取是哪个字段不合乎格局
				field, ok := uType.FieldByName(fieldName) //通过反射获取filed
				if ok {
					errorInfo := field.Tag.Get(validateErrTagName) //error_msg tag 值
					if errorInfo != "" {
						errs.Add(NewFieldError(fieldName, errorInfo))
						continue
					}
				}
				errs.Add(NewFieldError(fieldName, err.Translate(trans)))
			}
			if errs.IsError() {
				return errs
			}
		}
	}
	return nil
}
