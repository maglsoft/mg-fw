package unixTime

import (
	"database/sql/driver"
	"fmt"
	"strconv"
	"time"
)

// UnixTime is alias type for int64
type UnixTime int64

const (
	DateTimeFormatter = "2006-01-02 15:04:05"
	DateFormatter     = "2006-01-02"
	zone              = "Asia/Shanghai"
)

func Now() UnixTime {
	return UnixTime(time.Now().Unix())
}

func New(date string) (UnixTime, error) {
	var local, _ = time.LoadLocation(zone)
	now, err := time.ParseInLocation(DateTimeFormatter, date, local)
	if err != nil {
		if now, err = time.ParseInLocation(DateFormatter, date, local); err != nil {
			return 0, err
		}
	}
	return UnixTime(now.Unix()), nil
}

// UnmarshalJSON implements json unmarshal interface.
func (t *UnixTime) UnmarshalJSON(data []byte) (err error) {
	var local, _ = time.LoadLocation(zone)
	s := string(data)
	tmp, err := time.ParseInLocation("2006-01-02 15:04:05", s, local)
	if err != nil {
		*t = 0
	} else {
		*t = UnixTime(tmp.Unix())
	}
	return nil
}

func (t UnixTime) Date() time.Time {
	return time.Unix(t.RawVal(), 0)
}

// MarshalJSON implements json marshal interface.
func (t UnixTime) MarshalJSON() ([]byte, error) {
	b := make([]byte, 0, len(DateTimeFormatter)+2)
	b = append(b, '"')
	if t != 0 {
		t1 := time.Unix(int64(t), 0)
		byteTime := []byte(t1.Format(DateTimeFormatter))
		b = append(b, byteTime...)
	}
	b = append(b, '"')
	return b, nil
}

func (t UnixTime) String() string {
	return strconv.Itoa(int(t))
}

func (t UnixTime) local() time.Time {
	loc, _ := time.LoadLocation(zone)
	return time.Unix(int64(t), 0).In(loc)
}

func (t UnixTime) RawVal() int64 {
	return int64(t)
}

// Value insert timestamp into mysql need this function
func (t UnixTime) Value() (driver.Value, error) {
	return int64(t), nil
}

// Scan valueof time.Time 注意是指针类型 method
func (t *UnixTime) Scan(v interface{}) error {
	switch value := v.(type) {
	case time.Time:
		*t = UnixTime(value.Unix())
		return nil
	case int:
		*t = UnixTime(value)
		return nil
	case int64:
		*t = UnixTime(value)
		return nil
	case []byte:
		s := string(value)
		if n, err := strconv.Atoi(s); err == nil {
			*t = UnixTime(n)
		}
		return nil
	default:
		return fmt.Errorf("can not convert %v to timestamp", v)
	}
}
