package gtime

import (
	"database/sql/driver"
	"fmt"
	"strconv"
	"time"
)

// GTime is alias type for time.Time
type GTime time.Time

const (
	DateTimeFormatter = "2006-01-02 15:04:05"
	DateFormatter     = "2006-01-02"
	zone              = "Asia/Shanghai"
)

func New(date string) (GTime, error) {
	now, err := time.ParseInLocation(DateTimeFormatter, date, time.Local)
	if err != nil {
		if now, err = time.ParseInLocation(DateFormatter, date, time.Local); err != nil {
			return GTime{}, err
		}
	}
	t := GTime(now)
	return t, nil
}

// UnmarshalJSON implements json unmarshal interface.
func (t *GTime) UnmarshalJSON(data []byte) (err error) {
	now, err := time.ParseInLocation(DateTimeFormatter, string(data), time.Local)
	*t = GTime(now)
	return
}

// MarshalJSON implements json marshal interface.
func (t GTime) MarshalJSON() ([]byte, error) {
	b := make([]byte, 0, len(DateTimeFormatter)+2)
	b = append(b, '"')
	b = time.Time(t).AppendFormat(b, DateTimeFormatter)
	b = append(b, '"')
	return b, nil
}

func (t GTime) String() string {
	return time.Time(t).Format(DateTimeFormatter)
}

func (t GTime) local() time.Time {
	loc, _ := time.LoadLocation(zone)
	return time.Time(t).In(loc)
}

// Value insert timestamp into mysql need this function
func (t GTime) Value() (driver.Value, error) {
	var zeroTime time.Time
	var ti = time.Time(t)
	if ti.UnixNano() == zeroTime.UnixNano() {
		return nil, nil
	}
	return ti.Unix(), nil
}

// Scan valueof time.Time 注意是指针类型 method
func (t *GTime) Scan(v interface{}) error {
	switch value := v.(type) {
	case time.Time:
		*t = GTime(value)
		return nil
	case int:
		t1 := time.Unix(int64(value), 0)
		*t = GTime(t1)
		return nil
	case int64:
		t1 := time.Unix(value, 0)
		*t = GTime(t1)
		return nil
	case []byte:
		s := string(value)
		if n, err := strconv.Atoi(s); err == nil {
			t2 := time.Unix(int64(n), 0)
			*t = GTime(t2)
		}
		return nil
	default:
		return fmt.Errorf("can not convert %v to timestamp", v)
	}
}
