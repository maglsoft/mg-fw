package mysqldb

import (
	"github.com/pkg/errors"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

type BaseService struct {
}

var ErrModelOrTableParamValid = errors.New("BaseService.GetTable参数modelOrTable只支持schema.Tabler和string类型")
var ErrCheckExistsError = errors.New("BaseService.Exists方法调用出错")
var ErrRecordNotFound = errors.New("记录不存在")

func (svc *BaseService) GetTable(db *gorm.DB, modelOrTable any) (*gorm.DB, error) {
	switch v := modelOrTable.(type) {
	case schema.Tabler:
		return db.Model(v), nil
	case string:
		return db.Table(v), nil
	default:
		return db, ErrModelOrTableParamValid
	}
}

// Exists 检查数据是否存在
func (svc *BaseService) Exists(db *gorm.DB, modelOrTable any, exist *bool, where string, args ...any) error {
	query, err := svc.GetTable(db, modelOrTable)
	if err != nil {
		return err
	}
	var count int64
	if err := query.Where(where, args...).Count(&count).Error; err != nil {
		return errors.Wrap(err, "检查数据是否存在失败")
	}
	*exist = count > 0
	return nil
}

func (svc *BaseService) GetFieldValue(db *gorm.DB, modelOrTable any, field string, id any, where string, args ...any) error {
	query, err := svc.GetTable(db, modelOrTable)
	if err != nil {
		return errors.Wrap(err, "获取字段值失败")
	}
	var ids []any
	if err := query.Where(where, args...).Limit(1).Pluck(field, &ids).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return ErrRecordNotFound
		}
		return errors.Wrap(err, "获取字段"+field+"值失败")
	}
	if ids == nil || len(ids) == 0 {
		return ErrRecordNotFound
	}
	id = ids[0]
	return nil
}

func (svc *BaseService) GetFieldValues(db *gorm.DB, modelOrTable any, field string, where string, args ...any) (values []any, err error) {
	query, err := svc.GetTable(db, modelOrTable)
	if err != nil {
		return nil, err
	}
	values = make([]any, 0)
	if err := query.Where(where, args...).Limit(1).Pluck(field, &values).Error; err != nil {
		return nil, errors.Wrap(err, "获取字段"+field+"值失败")
	}
	return values, nil
}
