package db

import (
	"encoding/json"
	"github.com/spf13/viper"
)

type MySqlConnectionPool struct {
	MaxOpenConns int // 允许打开的最大连接数
	MaxIdleConns int // 允许空闲的最大连接数
}

type MySqlOptions struct {
	Host     string
	Port     int
	User     string
	DbName   string
	Password string
	Charset  string
	Loc      string
	Pool     *MySqlConnectionPool
	LogLevel string
}

func NewMySqlOptions(configRoot string, cfg *viper.Viper) *MySqlOptions {
	opt := &MySqlOptions{}
	opt.Host = cfg.GetString(configRoot + ".host")
	opt.Port = cfg.GetInt(configRoot + ".port")
	opt.User = cfg.GetString(configRoot + ".user")
	opt.DbName = cfg.GetString(configRoot + ".dbName")
	opt.Password = cfg.GetString(configRoot + ".password")
	opt.Charset = cfg.GetString(configRoot + ".charset")
	opt.Loc = cfg.GetString(configRoot + ".loc")
	opt.Pool = &MySqlConnectionPool{}
	opt.Pool.MaxOpenConns = cfg.GetInt(configRoot + ".pool.maxOpenConns")
	opt.Pool.MaxIdleConns = cfg.GetInt(configRoot + ".pool.maxIdleConns")
	return opt
}

func (mo *MySqlOptions) String() string {
	b, _ := json.Marshal(*mo)
	return string(b)
}
