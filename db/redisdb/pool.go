package redisdb

import (
	"fmt"
	"github.com/gomodule/redigo/redis"
	"time"
)

func InitPool(options *RedisOptions) (*redis.Pool, error) {
	c, err := redis.Dial("tcp", fmt.Sprintf("%s:%d", options.Addr, options.Port))
	if err != nil {
		return nil, err
	}
	defer c.Close()
	dbOption := redis.DialDatabase(options.DB)
	pwOption := redis.DialPassword(options.Password)
	if options.ConTimeout == 0 {
		options.ConTimeout = 10
	}
	readTimeout := redis.DialReadTimeout(time.Second * time.Duration(options.ConTimeout))
	writeTimeout := redis.DialWriteTimeout(time.Second * time.Duration(options.ConTimeout))
	conTimeout := redis.DialConnectTimeout(time.Second * time.Duration(options.ConTimeout))
	pool := &redis.Pool{ //实例化一个连接池
		MaxIdle: options.MaxIdle, //最初的连接数量
		// MaxActive:1000000,    //最大连接数量
		MaxActive:   options.MaxActive,                  //连接池最大连接数量,不确定可以用0（0表示自动定义），按需分配
		IdleTimeout: time.Duration(options.IdleTimeout), //连接关闭时间 300秒 （300秒不使用自动关闭）
		Wait:        true,
		Dial: func() (redis.Conn, error) { //要连接的redis数据库
			con, err := redis.Dial("tcp", fmt.Sprintf("%s:%d", options.Addr, options.Port), dbOption, pwOption,
				readTimeout, writeTimeout, conTimeout)
			if err != nil {
				return nil, err
			}
			return con, nil
		},
	}
	return pool, nil
}
