package redisdb

type RedisOptions struct {
	Addr        string `json:"addr"`
	Port        int    `json:"port"`
	Password    string `json:"password"`
	DB          int    `json:"db"`
	MaxActive   int    `json:"maxActive"`
	MaxIdle     int    `json:"maxIdle"`
	IdleTimeout int    `json:"idleTimeout"`
	ConTimeout  int    `json:"conTimeout"`
}
