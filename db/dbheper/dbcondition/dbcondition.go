package dbcondition

import "strings"

type DBCondition struct {
	wheres []string
	args   []any
}

const (
	AND = "AND"
	OR  = "OR"
)

func New() *DBCondition {
	return &DBCondition{
		wheres: make([]string, 0),
		args:   make([]any, 0),
	}
}

// Add 添加表达式项
func (dc *DBCondition) Add(where string, args ...any) *DBCondition {
	dc.wheres = append(dc.wheres, where)
	dc.args = append(dc.args, args...)
	return dc
}

// Build 生成表达式
func (dc *DBCondition) Build(condition ...string) (expr string, args []interface{}) {
	szJoin := " AND "
	if len(condition) > 0 && condition[0] != "" {
		szJoin = " " + condition[0] + " "
	}
	expr = strings.Join(dc.wheres, szJoin)
	return
}
