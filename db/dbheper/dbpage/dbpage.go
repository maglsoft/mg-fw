package dbpage

import "gorm.io/gorm"

type DataPage struct {
	Total     int         `json:"total"`
	PageCount int         `json:"pc"`
	PageIndex int         `json:"pi"`
	PageSize  int         `json:"ps"`
	Rows      interface{} `json:"rows"`
}

type DataPageT[T any] struct {
	Total     int `json:"total"`
	PageCount int `json:"pc"`
	PageIndex int `json:"pi"`
	PageSize  int `json:"ps"`
	Rows      []T `json:"rows"`
}

func NewT[T any]() *DataPageT[T] {
	return &DataPageT[T]{
		Total:     0,
		PageCount: 0,
		PageIndex: 0,
		PageSize:  0,
		Rows:      make([]T, 0, 0),
	}
}

func (dpt *DataPageT[T]) SetRow(rows []T) *DataPageT[T] {
	dpt.Rows = rows
	return dpt
}

func (dpt *DataPageT[T]) Set(total, pageIndex, pageSize int, rows []T) *DataPageT[T] {
	dpt.PageCount = CalcPageCount(total, pageSize)
	dpt.PageIndex = pageIndex
	dpt.PageSize = pageSize
	dpt.Rows = rows
	dpt.Total = total
	return dpt
}

func New() *DataPage {
	return &DataPage{
		Total:     0,
		PageCount: 0,
		PageIndex: 1,
		PageSize:  20,
	}
}

func New4(total, pageCount, pageIndex, pageSize int) *DataPage {
	return &DataPage{
		Total:     total,
		PageCount: pageCount,
		PageIndex: pageIndex,
		PageSize:  pageSize,
		Rows:      nil,
	}
}

func New5(total, pageCount, pageIndex, pageSize int, rows interface{}) *DataPage {
	return &DataPage{
		Total:     total,
		PageCount: pageCount,
		PageIndex: pageIndex,
		PageSize:  pageSize,
		Rows:      rows,
	}
}

// UpdatePageCount 更新页码数
func (dp *DataPage) UpdatePageCount(total, pageSize int) {
	dp.PageCount = CalcPageCount(total, pageSize)
	dp.PageSize = pageSize
}

// CalcPageCount 计算页码数
func CalcPageCount(total, pageSize int) int {
	if pageSize == 0 {
		return 0
	}
	n := total / pageSize
	if total%pageSize > 0 {
		n++
	}
	return n
}

// maxPageSize 最大返回行数
var maxPageSize = 300

// defaultPageSize 默认分页大小
var defaultPageSize = 50

// minPageIndex 最小页码
var minPageIndex = 1

// SetMaxPageSize 设置最大返回行数
func SetMaxPageSize(size int) {
	maxPageSize = size
}

// SetDefaultPageSize 设置默认分页大小
func SetDefaultPageSize(size int) {
	defaultPageSize = size
}

// SetMinPageIndex 设置页码开始
func SetMinPageIndex(pi int) {
	minPageIndex = pi
}

func ValidListParam(count *int) {
	if *count < 0 {
		*count = maxPageSize
	}
}

func ValidPageParam(pageIndex *int, pageSize *int) {
	if *pageIndex < minPageIndex {
		*pageIndex = minPageIndex
	}
	if *pageSize == 0 {
		*pageSize = defaultPageSize
	}
	if *pageSize > maxPageSize {
		*pageSize = maxPageSize
	}
}

// CalcPageOffset 计算页码记录偏移量, 页码从1开妈
func CalcPageOffset(pageIndex, pageSize int) int {
	return (pageIndex - minPageIndex) * pageSize
}

// GetQueryPage 查询分页
func GetQueryPage(query *gorm.DB, pageIndex, pageSize int) *gorm.DB {
	if query == nil {
		return nil
	}
	ValidPageParam(&pageIndex, &pageSize)
	offset := CalcPageOffset(pageIndex, pageSize)
	return query.Offset(offset).Limit(pageSize)
}
