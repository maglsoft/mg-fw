package middleware

import (
	"gitee.com/maglsoft/mg-fw/utils"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"strings"
)

func checkJwtTargetItem(target string, rule string) bool {
	if rule == "*" {
		return true
	}
	if target == "" && rule != "" {
		return false
	}
	return strings.Index(rule, target) != -1
}

func checkJwtTarget(target *utils.JwtTarget, rule *utils.JwtTarget) bool {
	if rule == nil {
		return true
	}
	if !checkJwtTargetItem(target.Issuer, rule.Issuer) {
		return false
	}
	if !checkJwtTargetItem(target.Subject, rule.Subject) {
		return false
	}
	return true
}

type GetAuthorizationFunc func(ctx *gin.Context) (utils.BaseAuthor, error)

type AuthorWareOptions struct {
	// SaveParamName context存储的键名
	SaveParamName string
	CookieName    string
	HeaderName    string
	FormName      string
	QueryName     string
	Author        utils.BaseAuthor
	Getter        GetAuthorizationFunc
}

func AuthorWare(options *AuthorWareOptions, jwt utils.JwtToken, rule *utils.JwtTarget) func(ctx *gin.Context) {
	if options.Author == nil {
		panic(errors.New("jwtToken参数未设置author"))
	}
	return func(ctx *gin.Context) {
		if options == nil {
			ctx.Next()
			return
		}
		if options.Getter != nil {
			author, err := options.Getter(ctx)
			if err != nil {
				ctx.AbortWithStatusJSON(403, map[string]any{
					"code": 403,
					"msg":  err.Error(),
				})
				return
			}
			if author != nil {
				ctx.Set(options.SaveParamName, author)
				ctx.Next()
				return
			}
		}
		var err error
		var token = ""
		if options.CookieName != "" {
			token, err = ctx.Cookie(options.CookieName)
			if err != nil {
				token = ""
			}
		}
		if options.FormName != "" {
			if token == "" {
				token = ctx.GetHeader(options.HeaderName)
			}
			if strings.ToUpper(ctx.Request.Method) == "POST" {
				if token == "" {
					token, _ = ctx.GetPostForm(options.FormName)
				}
			}
		}
		if options.QueryName != "" {
			if token == "" {
				token, _ = ctx.GetQuery(options.QueryName)
			}
		}
		if token == "" {
			ctx.AbortWithStatusJSON(403, map[string]any{
				"code": 403,
				"msg":  "未登录",
			})
			return
		}
		if author, tkTarget, err := jwt.DecodeJwtToken(token, options.Author); err != nil {
			ctx.AbortWithStatusJSON(403, map[string]any{
				"code": 403,
				"msg":  "令牌无效或已过期",
			})
			return
		} else {
			if !checkJwtTarget(tkTarget, rule) {
				ctx.AbortWithStatusJSON(200, map[string]any{
					"code": 403,
					"msg":  "无权操作",
				})
				return
			}
			ctx.Set(options.SaveParamName, author)
			ctx.Next()
		}
	}
}
