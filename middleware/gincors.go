package middleware

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

// GinCors gin跨域插件
func GinCors(headers ...string) gin.HandlerFunc {
	return func(c *gin.Context) {
		origin := c.Request.Header.Get("Origin")
		allowHeaders := strings.Builder{}
		allowHeaders.WriteString("Accept, Authorization, Content-Type, Content-Length, X-CSRF-Token, x-access-token, Token, session, Origin, Host, Connection, Accept-Encoding, Accept-Language, X-Requested-With")
		if len(headers) > 0 {
			allowHeaders.WriteString(strings.Join(headers, ","))
		}
		c.Writer.Header().Set("Access-Control-Allow-Origin", origin)
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE")
		c.Writer.Header().Set("Access-Control-Allow-Headers", allowHeaders.String())

		if c.Request.Method == http.MethodOptions {
			c.AbortWithStatus(http.StatusNoContent)
			return
		}
		c.Request.Header.Del("Origin")
		c.Next()
	}
}
