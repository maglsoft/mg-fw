package alibaba

import (
	"gzqlt-mb-go/pk/utils"
	"strconv"
	"testing"
	"time"
)

func TestAlibabaSms(t *testing.T) {
	outId := "order_" + strconv.Itoa(int(time.Now().Unix()))

	code := GenerateSmsCode(5)
	signName := "注册验证"
	templateCode := "SMS_6310594"
	mobile := "13668510783"
	content := NewVCodeSmsContent("贵州省黔林通", code)
	data := NewSmsData(signName, templateCode, content)
	sms, err := SmsSend(mobile, data, outId)
	if err != nil {
		t.Fatalf("发送验证短信失败:%s", err)
	}
	t.Logf("发送验证短信成功:%v", utils.JsonTryEncode(sms))
	details, err := GetSmsSendDetail(mobile, sms.BizId, 1)
	if err != nil {
		t.Fatalf("查询发送明细出错:%s", err)
	}
	t.Logf("明细:%s", utils.JsonTryEncode(details))
}

func TestGetSmsSendDetail(t *testing.T) {
	bizId := "791607057707580622^0"
	mobile := "13668510783"
	details, err := GetSmsSendDetail(mobile, bizId, 1)
	if err != nil {
		t.Fatalf("查询发送明细出错:%s", err)
	}
	t.Logf("明细:%s", utils.JsonTryEncode(details))
}
