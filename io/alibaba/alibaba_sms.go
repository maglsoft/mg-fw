package alibaba

import (
	"encoding/json"
	"errors"
	"fmt"
	"live-room-go/inner/config"
	alibSDK "github.com/aliyun/alibaba-cloud-sdk-go/sdk"
	alibCredentials "github.com/aliyun/alibaba-cloud-sdk-go/sdk/auth/credentials"
	"github.com/aliyun/alibaba-cloud-sdk-go/sdk/requests"
	dysmsapi "github.com/aliyun/alibaba-cloud-sdk-go/services/dysmsapi"
	"log"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

var accessKey = "你的accessKeyId" // accessKey
var accessSecret = ""           // accessKeySecret
var endpoint = "dysmsapi.aliyuncs.com"
var regionId = "cn-hangzhou"
var smsClient *dysmsapi.Client
var smsRequestScheme = "https"

type BaseSmsData struct {
	SignName     string
	TemplateCode string
	Content      interface{}
}

type SmsResponseData struct {
	Message   string `json:"message"`
	RequestId string `json:"requestId"`
	Code      string `json:"code"`
	BizId     string `json:"bizId"`
}

// NewSmsData 创建sms数据
func NewSmsData(signName, templateCode string, content interface{}) *BaseSmsData {
	return &BaseSmsData{
		SignName:     signName,
		TemplateCode: templateCode,
		Content:      content,
	}
}

func init() {
	_viper := config.GetConfig()
	accessKey = _viper.GetString("alibaba.sms.accessKey")
	accessSecret = _viper.GetString("alibaba.sms.accessSecret")

	if s := _viper.GetString("alibaba.sms.scheme"); s != "" {
		smsRequestScheme = s
	}
	if s := _viper.GetString("endpoint"); s != "" {
		endpoint = s
	}
	if s := _viper.GetString("regionId"); s != "" {
		regionId = s
	}
}

func checkSmsInit() {
	if accessKey == "" {
		panic("未配置参数alibaba.sms.accessKey")
	}
	if accessSecret == "" {
		panic(errors.New("未配置参数alibaba.sms.accessSecret"))
	}
	if smsClient == nil {
		smsConfig := alibSDK.NewConfig()
		credential := alibCredentials.NewAccessKeyCredential(accessKey, accessSecret)
		_client, err := dysmsapi.NewClientWithOptions(regionId, smsConfig, credential)
		if err != nil {
			panic(err)
		}
		smsClient = _client
	}
}

// SmsSend 发送短信
func SmsSend(mobile string, smsData *BaseSmsData, outId ...string) (*SmsResponseData, error) {
	checkSmsInit()
	if smsData == nil {
		return nil, errors.New("没有传入要发送的数据")
	}
	request := dysmsapi.CreateSendSmsRequest()
	request.Scheme = smsRequestScheme
	request.PhoneNumbers = mobile
	request.SignName = smsData.SignName
	request.TemplateCode = smsData.TemplateCode
	if len(outId) > 0 && outId[0] != "" {
		request.OutId = outId[0]
	}
	code := ""
	if smsData.Content != nil {
		bytes, err := json.Marshal(smsData.Content)
		if err != nil {
			return nil, err
		}
		code = string(bytes)
	}
	request.TemplateParam = code
	log.Printf("send outId:%s", request.OutId)

	sms, err := smsClient.SendSms(request)
	if err != nil {
		return nil, err
	}
	if !sms.IsSuccess() {
		return nil, err
	}
	if sms.Code != "OK" {
		return nil, errors.New(sms.Message)
	}
	log.Printf("sms:%v", sms)
	result := &SmsResponseData{}
	result.Code = sms.Code
	result.Message = sms.Message
	result.RequestId = sms.RequestId
	result.BizId = sms.BizId
	return result, err
}

// GetSmsSendDetail 获取短信发送明细
func GetSmsSendDetail(mobile string, bizId string, pageIndex int) ([]dysmsapi.SmsSendDetailDTO, error) {
	checkSmsInit()
	request := dysmsapi.CreateQuerySendDetailsRequest()
	request.BizId = bizId
	request.CurrentPage = requests.Integer(strconv.Itoa(pageIndex))
	request.PageSize = "10"
	request.SendDate = time.Now().Format("20060102")
	request.PhoneNumber = mobile
	res, err := smsClient.QuerySendDetails(request)
	if err != nil {
		return nil, err
	}
	if res.Code == "OK" {
		return res.SmsSendDetailDTOs.SmsSendDetailDTO, nil
	}
	return nil, errors.New(res.Message)
}

// GenerateSmsCode 生成验证码;length代表验证码的长度
func GenerateSmsCode(length int) string {
	numbers := [10]byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	rand.Seed(time.Now().Unix())
	var sb strings.Builder
	nLen := len(numbers)
	for i := 0; i < length; i++ {
		fmt.Fprintf(&sb, "%d", numbers[rand.Intn(nLen)])
	}
	return sb.String()
}
