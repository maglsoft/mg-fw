package alibaba

// VCodeSmsContent 注册验证短信参数体
type VCodeSmsContent struct {
	Product string `json:"product"`
	Code    string `json:"code"`
}

// NewVCodeSmsContent 创建注册验证短信
func NewVCodeSmsContent(product, code string) *VCodeSmsContent {
	return &VCodeSmsContent{
		Product: product,
		Code:    code,
	}
}
