package kafka_consumer

import (
	"fmt"
	cluster "gitee.com/maglsoft/mg-fw/vender/sarama-cluster"
	"github.com/Shopify/sarama"
	"github.com/pkg/errors"
	"strings"
	"sync/atomic"
)

type KafkaConsumerMsgHandler func(msg *sarama.ConsumerMessage) bool
type KafkaConsumerNotificationHandler func(notification *cluster.Notification)
type KafkaConsumerErrorHandler func(err error)

type KafkaConsumer struct {
	addressList         string
	consumerGroup       string
	topicList           []string
	close               chan bool
	closeState          int32
	client              *cluster.Consumer
	msgHandler          KafkaConsumerMsgHandler
	errorHandler        KafkaConsumerErrorHandler
	notificationHandler KafkaConsumerNotificationHandler
}

// New 创建kafka消息者
func New(addressList string, group string, topicList []string) *KafkaConsumer {
	result := &KafkaConsumer{
		addressList:   addressList,
		consumerGroup: group,
		topicList:     topicList,
		close:         make(chan bool, 1),
		closeState:    1,
		client:        nil,
	}
	atomic.StoreInt32(&result.closeState, 1)
	return result
}

func (kc *KafkaConsumer) SetMsgHandler(handler KafkaConsumerMsgHandler) {
	kc.msgHandler = handler
}

func (kc *KafkaConsumer) SetErrorHandler(handler KafkaConsumerErrorHandler) {
	kc.errorHandler = handler
}

func (kc *KafkaConsumer) SetNotificationHandler(handler KafkaConsumerNotificationHandler) {
	kc.notificationHandler = handler
}

func (kc *KafkaConsumer) init() error {
	clusterCfg := cluster.NewConfig()
	clusterCfg.Consumer.Return.Errors = true
	clusterCfg.Consumer.Offsets.Initial = sarama.OffsetOldest
	clusterCfg.Consumer.Offsets.AutoCommit.Enable = false
	clusterCfg.Group.Return.Notifications = true
	//这行代码是因为github.com/bsm/sarama-cluster不维护了，库会有bug，我这里只是写个demo，将就着用
	//clusterCfg.Consumer.Offsets.CommitInterval = 1 * time.Second
	clusterCfg.Version = sarama.V0_9_0_1
	// kafka消费端
	if err := clusterCfg.Validate(); err != nil {
		return errors.New(fmt.Sprintf("Kafka consumer config invalidate. config: %v. err: %v", *clusterCfg, err))
	}
	client, err := cluster.NewConsumer(strings.Split(kc.addressList, ","), kc.consumerGroup, kc.topicList, clusterCfg)
	if err != nil {
		return errors.New(fmt.Sprintf("Create kafka consumer error: %v. config: %v", err, clusterCfg))
	}
	kc.client = client
	return nil
}

func (kc *KafkaConsumer) Close() error {
	if n := atomic.LoadInt32(&kc.closeState); n > 0 {
		return nil
	}
	close(kc.close)
	atomic.StoreInt32(&kc.closeState, 1)
	return nil
}

func (kc *KafkaConsumer) Start() error {
	if n := atomic.LoadInt32(&kc.closeState); n == 0 {
		return errors.New("服务已运行")
	}
	if err := kc.init(); err != nil {
		return errors.Wrap(err, "启动消费者出错")
	}
	if kc.client == nil {
		return errors.New("未初始化")
	}
	atomic.StoreInt32(&kc.closeState, 0)
	for {
		n := atomic.LoadInt32(&kc.closeState)
		if n > 0 {
			return nil
		}
		select {
		case msg, ok := <-kc.client.Messages():
			if ok {
				//这里目前我是打印数据，线上可能是写入数据到db，写入成功之后我们手动提交offset
				if kc.msgHandler != nil && kc.msgHandler(msg) {
					//这里是临时保存offset
					kc.client.MarkOffset(msg, "")
					//手动提交offset
					kc.client.CommitOffsets()
				}
			}
		case err, more := <-kc.client.Errors():
			if more {
				if kc.errorHandler != nil {
					kc.errorHandler(err)
				}
			}
		case ntf, more := <-kc.client.Notifications():
			if more {
				if kc.notificationHandler != nil {
					kc.notificationHandler(ntf)
				}
			}
		case <-kc.close:
			return nil
		}
	}
}
