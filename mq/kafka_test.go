package mq

import (
	"github.com/Shopify/sarama"
	"gzqlt-mb-go/pk/mq/kafka_consumer"
	"gzqlt-mb-go/pk/mq/kafka_producer"
	cluster "gzqlt-mb-go/vender/sarama-cluster"
	"log"
	"strconv"
	"testing"
	"time"
)

var kafkaAddressList = "kafka1:9092,kafka2:9092,kafka3:9092"
var kafkaTopic = "test_go"
var consumerGroup = "test_group1"

func createProducer() (*kafka_producer.KafkaProducer, error) {
	producer := kafka_producer.New(kafkaAddressList)
	if err := producer.Init(); err != nil {
		return nil, err
	}
	return producer, nil
}

func consumerMsgHandler(msg *sarama.ConsumerMessage) bool {
	log.Println("recv msg:" + string(msg.Value) + ", offset:" + strconv.Itoa(int(msg.Offset)) + ", key=" + string(msg.Key) + ", topic=" + msg.Topic)
	return true
}

func consumerErrorHandler(err error) {
	log.Println("consumer recv error:" + err.Error())
}

func consumerNtfHandler(ntf *cluster.Notification) {
	log.Printf("ntf:%v\n", ntf.Type)
}

func createCustomer(t *testing.T) (*kafka_consumer.KafkaConsumer, error) {
	consumer := kafka_consumer.New(kafkaAddressList, consumerGroup, []string{kafkaTopic})
	if err := consumer.Start(); err != nil {
		return nil, err
	}
	consumer.SetMsgHandler(consumerMsgHandler)
	consumer.SetErrorHandler(consumerErrorHandler)
	consumer.SetNotificationHandler(consumerNtfHandler)
	go func(test *testing.T) {
		if err := consumer.Start(); err != nil {
			test.Fatalf("创建消费者出错:%s", err)
		}
	}(t)
	return consumer, nil
}

func produceMessage(t *testing.T) {
	producer, err := createProducer()
	if err != nil {
		t.Fatalf("创建生产者出错:%s", err)
	}
	n := 0
	go func() {
		for {
			select {
			case <-time.Tick(1 * time.Second):
				n++
				str := "这是消息" + strconv.Itoa(n)
				p, o, err := producer.SendMessage(str, kafkaTopic)
				if err != nil {
					t.Logf("发送消息出错:%s", err)
				} else {
					t.Logf("发送消息成功 partition:%d, offset:%d", p, o)
				}
			}
		}
	}()
}

func TestKafkaMq(t *testing.T) {
	// 创建生产者
	produceMessage(t)
	// 创建消息接收者
	_, err := createCustomer(t)
	if err != nil {
		return
	}
	t.Log("创建生产者，消费者成功.")
	select {}
}
