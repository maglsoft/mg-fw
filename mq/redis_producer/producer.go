package redis_producer

import (
	"context"
	"github.com/gomodule/redigo/redis"
	"log"
)

type redisProductTopicData struct {
	topic string
	data  string
}

type RedisProducer struct {
	ctx    context.Context
	dbPool *redis.Pool
	buffer chan *redisProductTopicData
}

func NewRedisProducer(ctx context.Context, db *redis.Pool) *RedisProducer {
	result := &RedisProducer{
		ctx:    ctx,
		dbPool: db,
		buffer: make(chan *redisProductTopicData, 256),
	}
	go result.run()
	return result
}

func (rp *RedisProducer) Publish(topic string, data string) {
	rp.buffer <- &redisProductTopicData{
		topic: topic,
		data:  data,
	}
}

func (rp *RedisProducer) publishMessage(data *redisProductTopicData) {
	if data == nil {
		return
	}
	con := rp.dbPool.Get()
	defer con.Close()
	if _, err := con.Do("PUBLISH", data.topic, data.data); err != nil {
		log.Printf("发布消息到%s出错", data.topic)
	}
}

func (rp *RedisProducer) run() {
	for {
		select {
		case <-rp.ctx.Done():
			return
		case data, ok := <-rp.buffer:
			if !ok {
				return
			}
			rp.publishMessage(data)
		}
	}
}
