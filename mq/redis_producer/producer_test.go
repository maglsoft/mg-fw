package redis_producer

import (
	"context"
	"fmt"
	"gzqlt-mb-go/pk/db/redisdb"
	"log"
	"os"
	"os/signal"
	"syscall"
	"testing"
	"time"
)

func TestNewRedisProducer(t *testing.T) {
	dbPool, err := redisdb.InitPool(&redisdb.RedisOptions{
		Addr:        "127.0.0.1",
		Port:        6379,
		Password:    "",
		DB:          10,
		MaxActive:   1000,
		MaxIdle:     10,
		IdleTimeout: 30,
	})
	if err != nil {
		t.Fatalf("init redis pool error:%s\n", err)
	}
	producer := NewRedisProducer(context.Background(), dbPool)
	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	i := 0
	go func() {
		<-quit
	}()
	for {
		select {
		case <-quit:
			goto exit
		default:
		}
		if i > 20 {
			goto exit
		}
		msg := fmt.Sprintf("这是发布的第%d条消息", i)
		log.Printf("生产消息:%s\n", msg)
		producer.Publish("test", []byte(msg))
		i++
		time.Sleep(1 * time.Second)
	}
exit:
	t.Logf("测试结束")
}
