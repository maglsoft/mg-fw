package redis_consumer

import (
	"context"
	"log"
	"testing"
	"time"
)

func TestNewRedisConsumer(t *testing.T) {
	ctx, _ := context.WithTimeout(context.Background(), 180*time.Second)
	//quit := make(chan os.Signal)
	//signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	consumer, err := NewRedisConsumer(ctx, &RedisConsumerOptions{
		Addr:     "127.0.0.1",
		Port:     6379,
		Password: "",
		DB:       10,
	})
	if err != nil {
		t.Fatalf(err.Error())
	}
	consumer.Subscribe("test", func(data string) {
		log.Printf("收到test渠道消息:%s\n", data)
	})
	consumer.Subscribe("test1", func(data string) {
		log.Printf("收到test1渠道消息:%s\n", data)
	})
	if err := consumer.Start(); err != nil {
		t.Fatalf("redis消息订阅出错:%s\n", err)
	}
	//<-quit
	//cancel()
	t.Logf("测试结束.")
}
