package net

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"github.com/spf13/cast"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"path"
	"strings"
)

func convertToQueryParamStr(params map[string][]string) string {
	if params == nil {
		return ""
	}
	sb := make([]string, 0, len(params))
	for k, r := range params {
		for _, v := range r {
			sb = append(sb, fmt.Sprintf("%s=%s", k, v))
		}
	}
	return strings.Join(sb, "&")
}

// HttpGetQueryMerge 合并get请求参数
func HttpGetQueryMerge(sUrl string, params map[string]string) (string, error) {
	uri, err := url.Parse(sUrl)
	if err != nil {
		return sUrl, errors.Wrap(err, "请求地址有误")
	}
	queryParams := make(map[string][]string)
	for k, v := range uri.Query() {
		queryParams[k] = v
	}
	if params != nil {
		for k, v := range params {
			if l, ok := queryParams[k]; ok {
				l = append(l, v)
				queryParams[k] = l
			} else {
				queryParams[k] = []string{v}
			}
		}
	}
	requestUrl := uri.RawPath
	szParams := convertToQueryParamStr(queryParams)
	if szParams != "" {
		requestUrl += "?" + url.QueryEscape(szParams)
	}
	return requestUrl, nil
}

// HttpRequest 发送网络请求
func HttpRequest(method string, sUrl string, body io.Reader, headers ...map[string]string) ([]byte, error) {
	var err error
	// 准备: HTTP请求
	httpReq, err := http.NewRequest(strings.ToUpper(method), sUrl, body)
	if err != nil {
		return nil, errors.Wrap(err, "发送请求出错")
	}
	if len(headers) > 0 && headers[0] != nil {
		for k, v := range headers[0] {
			httpReq.Header.Add(k, v)
		}
	}
	// DO: HTTP请求
	httpRsp, err := http.DefaultClient.Do(httpReq)
	if err != nil {
		return nil, errors.Wrap(err, "发送请求出错")
	}
	defer httpRsp.Body.Close()
	// Read: HTTP结果
	rspBody, err := ioutil.ReadAll(httpRsp.Body)
	if err != nil {
		return nil, errors.Wrap(err, "读取请求返回值出错")
	}
	if httpRsp.StatusCode < http.StatusOK || httpRsp.StatusCode > http.StatusIMUsed {
		return rspBody, errors.Errorf("(%d)%s", httpRsp.StatusCode, rspBody)
	}
	return rspBody, nil
}

// HttpPostJson 发送post请求
func HttpPostJson(sUrl string, params map[string]any, headers ...map[string]string) ([]byte, error) {
	header := make(map[string]string)
	header["Content-Type"] = "application/json"
	header["Accept"] = "application/json"
	if len(headers) > 0 && headers[0] != nil {
		for k, v := range headers[0] {
			header[k] = v
		}
	}
	var body io.Reader
	if params != nil {
		bs, err := json.Marshal(params)
		if err != nil {
			return nil, errors.Wrap(err, "编码失败")
		}
		body = bytes.NewReader(bs)
	}
	return HttpRequest("post", sUrl, body, header)
}

func HttpPutJson(sUrl string, params map[string]any, headers ...map[string]string) ([]byte, error) {
	header := make(map[string]string)
	header["Content-Type"] = "application/json"
	header["Accept"] = "application/json"
	if len(headers) > 0 && headers[0] != nil {
		for k, v := range headers[0] {
			header[k] = v
		}
	}
	var body io.Reader
	if params != nil {
		bs, err := json.Marshal(params)
		if err != nil {
			return nil, errors.Wrap(err, "编码失败")
		}
		body = bytes.NewReader(bs)
	}
	return HttpRequest("PUT", sUrl, body, header)
}

func HttpDeleteJson(sUrl string, params map[string]any, headers ...map[string]string) ([]byte, error) {
	header := make(map[string]string)
	header["Content-Type"] = "application/json"
	header["Accept"] = "application/json"
	if len(headers) > 0 && headers[0] != nil {
		for k, v := range headers[0] {
			header[k] = v
		}
	}
	var body io.Reader
	if params != nil {
		bs, err := json.Marshal(params)
		if err != nil {
			return nil, errors.Wrap(err, "编码失败")
		}
		body = bytes.NewReader(bs)
	}
	return HttpRequest("DELETE", sUrl, body, header)
}

func HttpGet(sUrl string, headers ...map[string]string) ([]byte, error) {
	req, err := http.NewRequest("GET", sUrl, nil)
	if err != nil {
		return nil, errors.Wrap(err, "发送请求出错")
	}
	if len(headers) > 0 && headers[0] != nil {
		for k, v := range headers[0] {
			req.Header.Add(k, v)
		}
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "发送请求出错")
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Wrap(err, "读取返回数据出错")
	}
	if resp.StatusCode != http.StatusOK {
		return body, errors.Wrap(err, string(body))
	}
	return body, nil
}

func HttpPostFileEx(sUrl string, paramName string, fileName string, params map[string]string, headers map[string]string) ([]byte, error) {
	r, w := io.Pipe()
	formWriter := multipart.NewWriter(w)
	go func() {
		defer w.Close()
		defer formWriter.Close()
		if params != nil {
			for k, v := range params {
				if e := formWriter.WriteField(k, v); e != nil {
					return
				}
			}
		}
		baseName := path.Base(fileName)
		part, err := formWriter.CreateFormFile(paramName, baseName)
		if err != nil {
			return
		}
		file, err := os.Open(fileName)
		if err != nil {
			return
		}
		defer file.Close()
		if n, err := io.Copy(part, file); err != nil {
			log.Printf("upload io.Copy file error:%s\n", err)
			return
		} else {
			log.Printf("upload io.copy len:%d\n", n)
		}
	}()
	req, err := http.NewRequest("POST", sUrl, r)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("请求地址[%s]失败", sUrl))
	}
	req.Header.Add("Content-type", formWriter.FormDataContentType())
	if headers != nil {
		for k, v := range headers {
			req.Header.Add(k, v)
		}
	}
	log.Printf("开始向%s提交请求\n", sUrl)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode < http.StatusOK || resp.StatusCode > http.StatusIMUsed {
		return respBody, errors.Errorf("(%d)%s", resp.StatusCode, resp.Status)
	}
	return respBody, nil
}

// HttpPostFile 上传文件
func HttpPostFile(sUrl string, paramName string, fileName string, fileReader io.Reader, params map[string]any, headers map[string]string) ([]byte, error) {
	bodyBuf := &bytes.Buffer{}
	bodyWriter := multipart.NewWriter(bodyBuf)
	if params != nil {
		for k, v := range params {
			if e := bodyWriter.WriteField(k, cast.ToString(v)); e != nil {
				return nil, e
			}
		}
	}
	//关键的一步操作
	fileWriter, err := bodyWriter.CreateFormFile(paramName, fileName)
	if err != nil {
		fmt.Println("error writing to buffer")
		return nil, err
	}

	//iocopy
	_, err = io.Copy(fileWriter, fileReader)
	if err != nil {
		return nil, err
	}

	contentType := bodyWriter.FormDataContentType()
	bodyWriter.Close()

	req, err := http.NewRequest("POST", sUrl, bodyBuf)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("请求地址[%s]失败", sUrl))
	}
	req.Header.Add("Content-type", contentType)
	if headers != nil {
		for k, v := range headers {
			req.Header.Add(k, v)
		}
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Printf("======>>> post err:%s, resp:%s\n", err, resp)
		return nil, err
	}
	defer resp.Body.Close()
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode < http.StatusOK || resp.StatusCode > http.StatusIMUsed {
		return respBody, errors.Errorf("(%d)%s", resp.StatusCode, resp.Status)
	}
	return respBody, nil
}
