package net

import (
	"encoding/json"
	"net/http"
)

type HttpRes struct {
	ContentType string `json:"contentType"`
	Status      string
	StatusCode  int
	Body        []byte
	Cookies     []*http.Cookie
}

func (hr *HttpRes) IsError() bool {
	return hr.StatusCode != http.StatusOK
}

func (hr *HttpRes) Unmarshal(res any) error {
	return json.Unmarshal(hr.Body, res)
}

func (hr *HttpRes) Text() string {
	return string(hr.Body)
}
