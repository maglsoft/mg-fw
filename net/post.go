package net

import (
	"encoding/json"
	"github.com/pkg/errors"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

func POST(url string, params any, cookies []*http.Cookie, handlers ...map[string]string) (*HttpRes, error) {
	reqData := []byte("")
	var err error
	if params != nil {
		// json.Marshal
		reqData, err = json.Marshal(params)
		if err != nil {
			log.Printf("post url[%s] error:%s\n", url, err)
			return nil, errors.Wrap(err, "处理请求参数出错")
		}
	}
	// 准备: HTTP请求
	reqBody := strings.NewReader(string(reqData))
	httpReq, err := http.NewRequest("POST", url, reqBody)
	if err != nil {
		log.Printf("post url[%s] error:%s\n", url, err)
		return nil, errors.Wrap(err, "发送请求出错")
	}
	httpReq.Header.Add("Content-Type", "application/json")
	if len(handlers) > 0 && handlers[0] != nil {
		for k, v := range handlers[0] {
			httpReq.Header.Add(k, v)
		}
	}

	if cookies != nil {
		for _, c := range cookies {
			httpReq.AddCookie(c)
		}
	}

	// DO: HTTP请求
	httpRsp, err := http.DefaultClient.Do(httpReq)
	if err != nil {
		log.Printf("post url[%s] error:%s\n", url, err)
		return nil, errors.Wrap(err, "发送请求出错")
	}
	defer httpRsp.Body.Close()
	// Read: HTTP结果
	rspBody, err := ioutil.ReadAll(httpRsp.Body)
	if err != nil {
		log.Printf("post url[%s] read response error:%s\n", url, err)
		return nil, errors.Wrap(err, "读取请求返回值出错")
	}
	httpRsp.Cookies()
	return &HttpRes{
		Cookies:     httpRsp.Cookies(),
		ContentType: httpRsp.Header.Get("content-type"),
		Status:      httpRsp.Status,
		StatusCode:  httpRsp.StatusCode,
		Body:        rspBody,
	}, nil
}
