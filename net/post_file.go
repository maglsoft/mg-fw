package net

import (
	"bytes"
	"fmt"
	"gitee.com/maglsoft/mg-fw/os/gfile"
	"github.com/pkg/errors"
	"github.com/spf13/cast"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
)

// PostFile 上传文件
func PostFile(filename string, targetUrl string, params map[string]any, headers map[string]string) (string, error) {
	if !gfile.Exists(filename) {
		return "", errors.Errorf("文件[%s]不存在", filename)
	}
	bodyBuf := &bytes.Buffer{}
	bodyWriter := multipart.NewWriter(bodyBuf)
	if params != nil {
		for k, v := range params {
			if e := bodyWriter.WriteField(k, cast.ToString(v)); e != nil {
				return "", e
			}
		}
	}
	//关键的一步操作
	fileWriter, err := bodyWriter.CreateFormFile("file", filename)
	if err != nil {
		fmt.Println("error writing to buffer")
		return "", err
	}

	//打开文件句柄操作
	fh, err := os.Open(filename)
	if err != nil {
		fmt.Println("error opening file")
		return "", err
	}
	defer fh.Close()

	//iocopy
	_, err = io.Copy(fileWriter, fh)
	if err != nil {
		return "", err
	}

	contentType := bodyWriter.FormDataContentType()
	bodyWriter.Close()

	req, err := http.NewRequest("POST", targetUrl, bodyBuf)
	if err != nil {
		return "", errors.Wrap(err, fmt.Sprintf("请求地址[%s]失败", targetUrl))
	}
	req.Header.Add("Content-type", contentType)
	if headers != nil {
		for k, v := range headers {
			req.Header.Add(k, v)
		}
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	if resp.StatusCode >= http.StatusOK && resp.StatusCode <= http.StatusIMUsed {
		return "", errors.Errorf("(%d)%s", resp.StatusCode, resp.Status)
	}
	defer resp.Body.Close()
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	return string(respBody), nil
}
