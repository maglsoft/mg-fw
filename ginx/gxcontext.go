package ginx

import (
	"fmt"
	"gitee.com/maglsoft/mg-fw/gtype"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/pkg/errors"
	"net/http"
	"strings"
	"time"
)

type gxResponseBody struct {
	Time    int64         `json:"t"`
	Code    ApiResultCode `json:"code"`
	Msg     string        `json:"msg,omitempty"`
	Content interface{}   `json:"content,omitempty"`
}

type GXContext struct {
	*gin.Context
	allParams gtype.GMap
}

type GxHandlerFunc func(*GXContext)

func parseGinParams(ctx *gin.Context, params gtype.GMap) error {
	bindParams := map[string]interface{}{}
	var tmpParams = make(map[string]string)
	// 获取get参数
	err2 := ctx.BindUri(&tmpParams)
	if err2 != nil {
		return errors.Wrap(err2, "解析url参数出错")
	}
	for k, v := range tmpParams {
		params[k] = v
	}
	// 混合 post参数
	if strings.ToUpper(ctx.Request.Method) == "POST" {
		contextType := ctx.Request.Header.Get("Content-Type")
		if strings.Index(strings.ToLower(contextType), "application/json") != -1 {
			err := ctx.ShouldBindBodyWith(&bindParams, binding.JSON)
			if err != nil {
				//报错
				return errors.Wrap(err, "解析json参数出错")
			}
			if len(bindParams) > 0 {
				for k, v := range bindParams {
					params[k] = v
				}
			}
		} else {
			_ = ctx.Request.ParseMultipartForm(32 << 20)
			if len(ctx.Request.PostForm) > 0 {
				for k, v := range ctx.Request.PostForm {
					params[k] = v[0]
				}
			}
		}
	}
	// 混合params参数
	for _, v := range ctx.Params {
		params[v.Key] = v.Value
	}
	return nil
}

func GxContextWrap(handler GxHandlerFunc) gin.HandlerFunc {
	return func(context *gin.Context) {
		params := make(gtype.GMap)
		if err := parseGinParams(context, params); err != nil {
			fmt.Printf("parse request params error:%v\n", err)
		}
		ctx := &GXContext{
			Context:   context,
			allParams: params,
		}
		defer func(gxCtx **GXContext) {
			ctx := *gxCtx
			if err := recover(); err != nil {
				if body, ok := err.(*ApiResponseData); ok {
					ctx.AbortWithStatusJSON(http.StatusOK, body)
				} else {
					fmt.Printf("服务错误:%v", err)
					ctx.Context.JSON(http.StatusOK, &gxResponseBody{
						Time:    time.Now().Unix(),
						Code:    ApiResultInnerErrCode,
						Msg:     "服务器内部错误",
						Content: nil,
					})
				}
			}
			(*gxCtx).Context = nil
			*gxCtx = nil
		}(&ctx)
		handler(ctx)
	}
}

func (c *GXContext) ReturnJson(code ApiResultCode, content any, msg ...string) {
	body := ApiResponseData{
		Time:    time.Now().UnixNano(),
		Code:    code,
		Msg:     "",
		Content: content,
	}

	if len(msg) > 0 && msg[0] != "" {
		body.Msg = msg[0]
	}
	panic(&body)
}

func (c *GXContext) ReturnSuccess(content interface{}, msg ...string) {
	c.ReturnJson(ApiResultSuccessCode, content, msg...)
}

func (c *GXContext) ReturnError(msg string, code ...ApiResultCode) {
	n := ApiResultAppErrCode
	if len(code) > 0 && code[0] > 0 {
		n = code[0]
	}
	c.ReturnJson(n, nil, msg)
}

func (c *GXContext) ReturnErrors(msg string, errors error, code ...ApiResultCode) {
	n := ApiResultAppErrCode
	if len(code) > 0 && code[0] > 0 {
		n = code[0]
	}
	c.ReturnJson(n, errors, msg)
}

func (c *GXContext) ReturnAuthFail() {
	c.ReturnJson(ApiResultAuthFailErrCode, nil, "未签权")
}

func (c *GXContext) ReturnForbiddenError() {
	c.ReturnJson(ApiResultForbiddenErrCode, nil, "签权无效或无权操作")
}

func (c *GXContext) ReturnInnerError() {
	c.ReturnJson(ApiResultInnerErrCode, nil, "服务器内部错误")
}

func (c *GXContext) ReturnNotfoundError() {
	c.ReturnJson(ApiResultNotfoundErrCode, nil, "地址不存在")
}

func (c *GXContext) BindParams(params any) error {
	return c.allParams.BindTo(params)
}

func (c *GXContext) AllParams() gtype.GMap {
	return c.allParams
}
