package ginx

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

type ApiResultCode int

const (
	ApiResultSuccessCode      ApiResultCode = 0
	ApiResultAppErrCode       ApiResultCode = 1
	ApiResultAuthFailErrCode  ApiResultCode = 401
	ApiResultForbiddenErrCode ApiResultCode = 403
	ApiResultNotfoundErrCode  ApiResultCode = 404
	ApiResultInnerErrCode     ApiResultCode = 500
)

type ApiCls struct{}

var Api = ApiCls{}

// ApiResponseData api接口返回的结果：
type ApiResponseData struct {
	Time    int64         `json:"t"`
	Code    ApiResultCode `json:"code"`
	Msg     string        `json:"msg,omitempty"`
	Content interface{}   `json:"content,omitempty"`
}

func (api *ApiCls) ReturnJson(c *gin.Context, code ApiResultCode, content any, msg ...string) {
	body := ApiResponseData{
		Time:    time.Now().Unix(),
		Code:    code,
		Msg:     "",
		Content: content,
	}
	if len(msg) > 0 && msg[0] != "" {
		body.Msg = msg[0]
	}
	c.JSON(http.StatusOK, &body)
}

// ReturnSuccess 成功
func (api *ApiCls) ReturnSuccess(c *gin.Context, content interface{}, msg ...string) {
	api.ReturnJson(c, ApiResultSuccessCode, content, msg...)
}

// ReturnError 出错
func (api *ApiCls) ReturnError(c *gin.Context, msg string, code ...ApiResultCode) {
	n := ApiResultAppErrCode
	if len(code) > 0 && code[0] > 0 {
		n = code[0]
	}
	api.ReturnJson(c, n, nil, msg)
}

func (api *ApiCls) ReturnAuthorFailError(c *gin.Context) {
	api.ReturnJson(c, ApiResultAuthFailErrCode, nil, "未签权")
}

func (api *ApiCls) ReturnForbiddenError(c *gin.Context) {
	api.ReturnJson(c, ApiResultForbiddenErrCode, nil, "签权无效或无权操作")
}

func (api *ApiCls) ReturnInnerError(c *gin.Context) {
	api.ReturnJson(c, ApiResultInnerErrCode, nil, "服务器内部错误")
}

func (api *ApiCls) ReturnNotfoundError(c *gin.Context) {
	api.ReturnJson(c, ApiResultNotfoundErrCode, nil, "地址不存在")
}
