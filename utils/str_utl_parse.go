package utils

import "strings"

// ParseUrlQuery 解析url参数
func ParseUrlQuery(urlQuery string) map[string]any {
	queryStr := urlQuery
	if idx := strings.Index(urlQuery, "?"); idx != -1 {
		queryStr = urlQuery[idx+1:]
	}
	items := strings.Split(queryStr, "&")
	result := make(map[string]any)
	for _, s := range items {
		pairs := strings.Split(s, "=")
		k := pairs[0]
		v := ""
		if len(pairs) > 1 {
			v = pairs[1]
		}
		if arr, ok := result[k].([]string); ok {
			result[k] = append(arr, v)
		} else if s, ok := result[k].(string); ok {
			arr := make([]string, 2, 2)
			arr[0] = s
			arr[1] = v
			result[k] = arr
		} else {
			result[k] = v
		}
	}
	return result
}
