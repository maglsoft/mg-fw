package utils

import (
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"log"
	"time"
)

type JwtTokenOptions struct {
	TokenJwtSecret string            `json:"tokenJwtSecret"`
	TokenJwtExp    time.Duration     `json:"tokenJwtExp"`
	TokenJwtAlg    jwt.SigningMethod `json:"tokenJwtAlg"`
}

const (
	DefaultJwtSecret = "607510af2d25158d055686c7"
	DefaultJwtExpSec = 60 * 60 * 24
)

var DefaultJwtAlg = jwt.SigningMethodHS256

//var defaultJwtTokenOptions = &JwtTokenOptions{
//	TokenJwtSecret: "607510af2d25158d055686c7",
//	TokenJwtExp:    60 * 60 * 24, // unit second
//	TokenJwtAlg:    nil,
//}

type TokenPayload struct {
	Payload BaseAuthor `json:"payload"`
	jwt.StandardClaims
}

//// SetJwtTokenOptions 设置jwtToken参数
//func SetJwtTokenOptions(options JwtTokenOptions) {
//	defaultJwtTokenOptions.TokenJwtSecret = options.TokenJwtSecret
//	defaultJwtTokenOptions.TokenJwtExp = options.TokenJwtExp
//	defaultJwtTokenOptions.TokenJwtAlg = options.TokenJwtAlg
//}

type JwtToken interface {
	EncodeJwtToken(author BaseAuthor, target *JwtTarget, tokenSecret ...string) (string, error)
	DecodeJwtToken(tokenStr string, author BaseAuthor, tokenSecret ...string) (BaseAuthor, *JwtTarget, error)
}

type jwtToken struct {
	opts *JwtTokenOptions
}

var algMaps = map[string]jwt.SigningMethod{
	"HS256": jwt.SigningMethodHS256,
	"HS382": jwt.SigningMethodHS384,
	"HS512": jwt.SigningMethodHS512,
	"RS256": jwt.SigningMethodRS256,
	"RS382": jwt.SigningMethodRS384,
	"RS512": jwt.SigningMethodRS512,
	"ES256": jwt.SigningMethodES256,
	"ES384": jwt.SigningMethodES384,
	"ES512": jwt.SigningMethodES512,
	"PS256": jwt.SigningMethodPS256,
	"PS384": jwt.SigningMethodPS384,
	"PS512": jwt.SigningMethodPS512,
}

func NewTokenJwtAlg(name string) jwt.SigningMethod {
	if a, ok := algMaps[name]; ok {
		return a
	}
	return DefaultJwtAlg
}

func NewJwtToken(options *JwtTokenOptions) JwtToken {
	if options == nil {
		options = &JwtTokenOptions{
			TokenJwtSecret: DefaultJwtSecret,
			TokenJwtExp:    DefaultJwtExpSec,
			TokenJwtAlg:    DefaultJwtAlg,
		}
	}
	if options.TokenJwtAlg == nil {
		options.TokenJwtAlg = DefaultJwtAlg
	}
	return &jwtToken{
		opts: options,
	}
}

type JwtTarget struct {
	Issuer  string `json:"issuer"`
	Subject string `json:"subject"`
}

func (jt *JwtTarget) String() string {
	if jt == nil {
		return ""
	}
	return fmt.Sprintf("iss=%s,sub=%s", jt.Issuer, jt.Subject)
}

func (jt *jwtToken) EncodeJwtToken(author BaseAuthor, target *JwtTarget, tokenSecret ...string) (string, error) {
	now := time.Now()
	expireTime := now.Add(jt.opts.TokenJwtExp * time.Second)

	claims := TokenPayload{
		Payload: author,
		StandardClaims: jwt.StandardClaims{
			//Audience:  "",                // 标识token的接收者
			ExpiresAt: expireTime.Unix(), // 过期时间.通常与Unix UTC时间做对比过期后token无效
			//Id:        "",                // 自定义的id号
			IssuedAt: now.Unix(), // 签名发行时间
			//Issuer:    "",                // 签名的发行者
			//NotBefore: 0,                 // 这条token信息生效时间.这个值可以不设置,但是设定后,一定要大于当前Unix UTC,否则token将会延迟生效.
			//Subject: "", // 签名面向的用户
		},
	}
	if target != nil {
		claims.StandardClaims.Issuer = target.Issuer
		claims.StandardClaims.Subject = target.Subject
	}
	token := jwt.NewWithClaims(jt.opts.TokenJwtAlg, claims)
	secret := jt.opts.TokenJwtSecret
	if len(tokenSecret) > 0 && tokenSecret[0] != "" {
		secret = tokenSecret[0]
	}
	log.Printf("secret:%s\n", secret)
	tokenString, err := token.SignedString([]byte(secret))
	if err != nil {
		log.Printf("SignedString error:%s\n", err)
		return "", err
	}
	return tokenString, nil
}

// DecodeJwtToken 验证token
func (jt *jwtToken) DecodeJwtToken(tokenStr string, author BaseAuthor, tokenSecret ...string) (BaseAuthor, *JwtTarget, error) {
	claims := &TokenPayload{
		Payload:        author,
		StandardClaims: jwt.StandardClaims{},
	}
	token, err := jwt.ParseWithClaims(tokenStr, claims, func(token *jwt.Token) (i interface{}, err error) {
		if len(tokenSecret) > 0 && tokenSecret[0] != "" {
			return []byte(tokenSecret[0]), nil
		}
		return []byte(jt.opts.TokenJwtSecret), nil
	})
	if err != nil {
		log.Printf("decode token error:%s\n", err)
		return nil, nil, err
	}
	if !token.Valid {
		return nil, nil, errors.New("令牌无效")
	}
	return claims.Payload, &JwtTarget{
		Issuer:  claims.Issuer,
		Subject: claims.Subject,
	}, nil
}
