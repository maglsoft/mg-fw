package utils

import (
	"log"
	"testing"
)

const str1 = "this is en params,params1=[param1][param2],param3=[param3]"
const str2 = "这是参数处理测试[[参数1]][[参数2]]=其他内容，参数参数3=[[参数3]]"
const testReplaceResult1 = "this is en params,params1=值1值2,param3=值3"
const testReplaceResult2 = "这是参数处理测试值1值2=其他内容，参数参数3=值3"

func TestStrFindSubIndexes(t *testing.T) {
	subItems := StrSearchSubs(str1, "[", "]")
	if len(subItems) != 3 || subItems[0].Sub != "param1" || subItems[1].Sub != "param2" || subItems[2].Sub != "param3" {
		t.Fatalf("英文参数提取不正确")
	}
	t.Logf("subItems:%v\n", subItems)
	for _, r := range subItems {
		s := str1[r.StartIdx:r.EndIdx]
		log.Printf("英文参数 index %d-%d:%s", r.StartIdx, r.EndIdx, s)
	}
	subItems1 := StrSearchSubs(str2, "[[", "]]")
	if len(subItems1) < 3 || subItems1[0].Sub != "参数1" || subItems1[1].Sub != "参数2" || subItems1[2].Sub != "参数3" {
		t.Fatalf("汉字参数提交不正确.")
	}
	t.Logf("subItems1:%v\n", subItems1)
	for _, r := range subItems1 {
		s := string([]rune(str2)[r.StartIdx:r.EndIdx])
		log.Printf("汉字参数 index %d-%d:%s", r.StartIdx, r.EndIdx, s)
	}
}

func TestStrReplaceSearch(t *testing.T) {
	params := map[string]string{
		"param1": "值1",
		"param2": "值2",
		"param3": "值3",
	}
	params1 := map[string]string{
		"参数1": "值1",
		"参数2": "值2",
		"参数3": "值3",
	}

	rpStr := StrReplaceSearch(str1, "[", "]", func(sub string) string {
		s, ok := params[sub]
		if ok {
			return s
		}
		return "unknown"
	})
	if rpStr != testReplaceResult1 {
		t.Fatalf("英文替换不正确:%s\n", rpStr)
	}
	t.Logf("英文替换后:%s\n", rpStr)

	rpStr1 := StrReplaceSearch(str2, "[[", "]]", func(sub string) string {
		if s, ok := params1[sub]; ok {
			return s
		}
		return "unknown"
	})
	if rpStr1 != testReplaceResult2 {
		t.Fatalf("汉字替换不正确:%s\n", rpStr1)
	}
	log.Printf("汉字替换后:%s\n", rpStr1)
}

func TestStrReplaceSearch2(t *testing.T) {
	s := "rtmp://live.freetable.cn/[d_app]/[d_stream]?id=[stream_id]&key=[key]&type=[type]"
	params := map[string]string{
		"d_app":    "live",
		"vhost":    "live.freetable.cn",
		"d_stream": "stream1",
		"key":      "key1",
		"type":     "relay",
	}
	s1 := StrReplaceSearch(s, "[", "]", func(sub string) string {
		if s1, ok := params[sub]; ok {
			return s1
		} else {
			return sub
		}
	})
	t.Logf("s:%s\n", s1)
}
