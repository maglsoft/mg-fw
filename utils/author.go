package utils

type BaseAuthor interface {
	GetUserId() string
	GetNickname() string
}

type SimpleBaseAuthor struct {
	UserId   string `json:"userId"`
	NickName string `json:"nickName"`
}

func (s *SimpleBaseAuthor) GetUserId() string {
	return s.UserId
}

func (s *SimpleBaseAuthor) GetNickname() string {
	return s.NickName
}

var _ BaseAuthor = (*SimpleBaseAuthor)(nil) // 用于验证是否实现接口
