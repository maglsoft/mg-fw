package utils

import (
	"encoding/json"
	"net/url"
	"strconv"
	"strings"
)

type StrSliceFilter func(s string) bool

// FilterStrSlice 过滤字符串数组
func FilterStrSlice(items []string, filter StrSliceFilter) []string {
	if items == nil {
		return nil
	}
	if filter == nil {
		return items
	}
	result := make([]string, 0, len(items))
	for _, s := range items {
		if filter(s) {
			result = append(result, s)
		}
	}
	return result
}

func StrSliceIndexOf(items []string, substr string) int {
	if len(items) == 0 {
		return -1
	}
	for i, s := range items {
		if s == substr {
			return i
		}
	}
	return -1
}

// FilterStrSliceDuplicate 去掉重复项，并按filter过滤返回结果
func FilterStrSliceDuplicate(items []string, filter StrSliceFilter) []string {
	if items == nil {
		return nil
	}
	existItems := make(map[string]interface{})
	result := make([]string, 0, len(items))
	for _, s := range items {
		if existItems[s] != nil {
			continue
		}
		existItems[s] = true
		if filter != nil {
			if filter(s) {
				result = append(result, s)
			}
		} else {
			result = append(result, s)
		}
	}
	return result
}

// EncodeURIComponent url编码
func EncodeURIComponent(str string) string {
	r := url.QueryEscape(str)
	r = strings.Replace(r, "+", "%20", -1)
	return r
}

// DecodeURLComponent url解码
func DecodeURLComponent(str string) string {
	r := url.QueryEscape(str)
	r = strings.Replace(r, "+", "%20", -1)
	return r
}

// StrVal 获取变量的字符串值
// 浮点型 3.0将会转换成字符串3, "3"
// 非数值或字符类型的变量将会被转换成JSON格式字符串
func StrVal(value interface{}) string {
	// interface 转 string
	var key string
	if value == nil {
		return key
	}
	switch value.(type) {
	case float64:
		ft := value.(float64)
		key = strconv.FormatFloat(ft, 'f', -1, 64)
	case float32:
		ft := value.(float32)
		key = strconv.FormatFloat(float64(ft), 'f', -1, 64)
	case int:
		it := value.(int)
		key = strconv.Itoa(it)
	case uint:
		it := value.(uint)
		key = strconv.Itoa(int(it))
	case int8:
		it := value.(int8)
		key = strconv.Itoa(int(it))
	case uint8:
		it := value.(uint8)
		key = strconv.Itoa(int(it))
	case int16:
		it := value.(int16)
		key = strconv.Itoa(int(it))
	case uint16:
		it := value.(uint16)
		key = strconv.Itoa(int(it))
	case int32:
		it := value.(int32)
		key = strconv.Itoa(int(it))
	case uint32:
		it := value.(uint32)
		key = strconv.Itoa(int(it))
	case int64:
		it := value.(int64)
		key = strconv.FormatInt(it, 10)
	case uint64:
		it := value.(uint64)
		key = strconv.FormatUint(it, 10)
	case string:
		key = value.(string)
	case []byte:
		key = string(value.([]byte))
	default:
		newValue, _ := json.Marshal(value)
		key = string(newValue)
	}
	return key
}
