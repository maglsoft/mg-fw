package utils

type StrScanner interface {
	Next() bool
	Prev() bool
	Read(readLen int) string
	ReadRune() *rune
	Current() int
	Inc(step int)
	Dec(step int)
}

type strScanner struct {
	buf      []rune
	startIdx int
	endIdx   int
}

func NewStrScanner(str string) StrScanner {
	buf := []rune(str)
	endIdx := len(buf)
	startIdx := -1
	return &strScanner{
		buf:      buf,
		endIdx:   endIdx,
		startIdx: startIdx,
	}
}

func (s *strScanner) Current() int {
	return s.startIdx
}

func (s *strScanner) Inc(step int) {
	if endIdx := s.startIdx + step; endIdx > s.endIdx {
		s.startIdx = endIdx
	} else {
		s.startIdx = endIdx
	}
}

func (s *strScanner) Dec(step int) {
	if endIdx := s.startIdx - step; endIdx < 0 {
		s.startIdx = 0
	} else {
		s.startIdx = endIdx
	}
}

func (s *strScanner) Next() bool {
	s.startIdx++
	return s.startIdx < s.endIdx
}

func (s *strScanner) Prev() bool {
	s.startIdx--
	return s.startIdx >= 0
}

func (s *strScanner) ReadRune() *rune {
	if s.startIdx < s.endIdx {
		return &s.buf[s.startIdx]
	}
	return nil
}

func (s *strScanner) Read(readLen int) string {
	endIdx := s.startIdx + readLen
	if endIdx > s.endIdx {
		endIdx = s.endIdx
	}
	return string(s.buf[s.startIdx:endIdx])
}
