package utils

// SliceFindIndex 在slice中查找索引序号，不存在时返回-1
func SliceFindIndex[T any](rows []T, comparer func(v T, idx ...int) bool) int {
	for i, v := range rows {
		if comparer(v, i) {
			return i
		}
	}
	return -1
}

// SliceFind 在slice中查找某一项
func SliceFind[T any](rows []T, comparer func(v T, idx ...int) bool) *T {
	for i, v := range rows {
		if comparer(v, i) {
			return &v
		}
	}
	return nil
}

func SliceFilter[T any](rows []T, comparer func(v T, idx ...int) bool) []T {
	result := make([]T, 0, len(rows))
	for i, v := range rows {
		if comparer(v, i) {
			result = append(result, v)
		}
	}
	return result
}

func StrSliceFilterEmpty(rows *[]string) *[]string {
	i := 0
	for _, s := range *rows {
		if s != "" {
			(*rows)[i] = s
			i++
		}
	}
	*rows = (*rows)[:i]
	return rows
}
