package utils

import "testing"

func TestStrScanner(t *testing.T) {
	str := "1234567890"
	t.Logf("===>:%s\n", str[0:2])
	scanner := NewStrScanner(str)
	readLen := 2
	secResults := []string{
		"12",
		"23",
		"34",
		"45",
		"56",
		"67",
		"78",
		"89",
		"90",
		"0",
	}
	i := 0
	for scanner.Next() {
		s := scanner.Read(readLen)
		if s != secResults[i] {
			t.Fatalf("next error index:%d need:%s,result:%s", i, secResults[i], s)
		} else {
			t.Logf("next index:%d, str:%s\n", i, s)
		}
		i++
	}
	i = len(secResults)
	for scanner.Prev() {
		i--
		s := scanner.Read(readLen)
		if s != secResults[i] {
			t.Fatalf("prev error index:%d need:%s,result:%s", i, secResults[i], s)
		} else {
			t.Logf("prev index:%d, str:%s\n", i, s)
		}
	}
	t.Logf("test strScanner next,read success.")
}
