package utils

import (
	"fmt"
	"strings"
)

type SubStrIndex struct {
	Sub      string
	StartIdx int
	EndIdx   int
}

func (si *SubStrIndex) String() string {
	return fmt.Sprintf("{ sub=%s, start=%d,end=%d }", si.Sub, si.StartIdx, si.EndIdx)
}

type SubReplacer func(sub string) string

func StrSearchSubs(str string, startStr, endStr string) []*SubStrIndex {
	scanner := NewStrScanner(str)
	startLen := len(startStr)
	endLen := len(endStr)
	nodeStack := make([]*SubStrIndex, 0)
	readBuf := strings.Builder{}
	result := make([]*SubStrIndex, 0)
	for scanner.Next() {
		testStart := scanner.Read(startLen)
		testEnd := scanner.Read(endLen)
		if testStart == startStr {
			// 开始
			nodeStack = append(nodeStack, &SubStrIndex{
				Sub:      "",
				StartIdx: scanner.Current() + startLen,
				EndIdx:   -1,
			})
			readBuf = strings.Builder{}
			scanner.Inc(startLen - 1)
		} else if testEnd == endStr {
			if len(nodeStack) == 0 {
				readBuf = strings.Builder{}
			} else {
				node := nodeStack[len(nodeStack)-1]
				nodeStack = nodeStack[:len(nodeStack)-1]
				node.Sub = readBuf.String()
				node.EndIdx = scanner.Current()
				readBuf = strings.Builder{}
				result = append(result, node)
				scanner.Inc(endLen - 1)
			}
		} else {
			c := scanner.ReadRune()
			if c != nil {
				readBuf.WriteRune(*c)
			}
		}
	}
	return result
}

func StrReplaceSearch(str string, startStr, endStr string, replace SubReplacer) string {
	items := StrSearchSubs(str, startStr, endStr)
	buf := []rune(str)
	result := strings.Builder{}
	startLen := len(startStr)
	endLen := len(endStr)
	i := 0
	for _, r := range items {
		s := replace(r.Sub)
		if r.StartIdx > i {
			result.WriteString(string(buf[i : r.StartIdx-startLen]))
		}
		i = r.EndIdx + endLen
		result.WriteString(s)
	}
	result.WriteString(string(buf[i:]))
	return result.String()
}
