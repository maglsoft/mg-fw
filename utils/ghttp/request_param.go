package ghttp

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"log"
	"strings"
	"sync"
)

type RequestParam struct {
	ctx    *gin.Context
	lock   sync.Mutex
	params map[string]interface{}
}

func NewRequestParams(c *gin.Context) *RequestParam {
	return &RequestParam{
		ctx:    c,
		params: make(map[string]interface{}),
	}
}

func ParseRequestParam(c *gin.Context) (*RequestParam, bool) {
	result := NewRequestParams(c)
	return result, result.Parse()
}

func BindRequestParam(c *gin.Context, bind interface{}) error {
	result := NewRequestParams(c)
	if ok := result.Parse(); ok {
		return result.Bind(bind)
	}
	return errors.Errorf("解析参数出错")
}

func (rp *RequestParam) Parse() bool {
	ctx := rp.ctx
	bindParams := map[string]interface{}{}
	var tmpParams = make(map[string]string)
	// 获取get参数
	err2 := ctx.ShouldBind(&tmpParams)
	if err2 != nil {
		fmt.Println(err2)
		return false
	}
	for k, v := range tmpParams {
		rp.Add(k, v)
	}
	// 混合 post参数
	if ctx.Request.Method == "POST" {
		contextType := ctx.Request.Header.Get("Content-Type")
		if contextType == "application/json" {
			err := ctx.ShouldBindBodyWith(&bindParams, binding.JSON)
			if err != nil { //报错
				log.Printf("nyx_request_mid_error %v,err: %v \n", bindParams, err)
				return false
			}
			if len(bindParams) > 0 {
				for k, v := range bindParams {
					rp.Add(k, v)
				}
			}
		} else {
			_ = ctx.Request.ParseMultipartForm(32 << 20)
			if len(ctx.Request.PostForm) > 0 {
				for k, v := range ctx.Request.PostForm {
					rp.Add(k, v[0])
				}
			}
		}
	}
	// 混合params参数
	for _, v := range ctx.Params {
		rp.Add(v.Key, v.Value)
	}
	return true
}

// Add 添加参数
func (rp *RequestParam) Add(key string, value interface{}) {
	rp.lock.Lock()
	rp.params[key] = value
	rp.lock.Unlock()
}

func (rp *RequestParam) getParams() map[string]interface{} {
	return rp.params
}

func (rp *RequestParam) Bind(result interface{}) error {
	log.Printf("rp.params:%v", rp.params)
	return mapstructure.Decode(rp.params, result)
}

// GetHeaderByKey 获取kname
func (rp *RequestParam) GetHeaderByKey(kname string) string {
	return rp.ctx.Request.Header.Get(kname)
}

// GetRemoteAddr 获取remoteAddr
func (rp *RequestParam) GetRemoteAddr() string {
	remoteAddr := rp.ctx.Request.Header.Get("Remote_addr") //远程调用
	if remoteAddr == "" {
		remoteAddr = rp.ctx.Request.Header.Get("X-Forward-For")
	}
	return remoteAddr
}

//AddParams 添加参数
func (rp *RequestParam) AddParams(m map[string]interface{}) {
	if len(m) <= 0 {
		return
	}
	for k, v := range m {
		rp.Add(k, v)
	}
}

// Join2UrlQuery json参数专urlQuery参数
func (rp *RequestParam) Join2UrlQuery() string {
	var params []string
	if len(rp.params) > 0 {
		for k, v := range rp.params {
			params = append(params, fmt.Sprintf("%s=%v", k, v))
		}
	}
	return strings.Join(params, "&")
}

// Delete 删除参数
func (rp *RequestParam) Delete(key string) {
	rp.lock.Lock()
	if _, ok := rp.params[key]; ok {
		delete(rp.params, key)
	}
	rp.lock.Unlock()
}
