package utils

import (
	"errors"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"
)

type CrossAppAuthor struct {
	AK string `json:"ak"`
	SimpleBaseAuthor
	AvatarUrl string `json:"avatarUrl"`
}

func createAuthorDataSign(author *CrossAppAuthor, secret string, expireTime string) string {
	data := author.AK + author.UserId + author.NickName + author.AvatarUrl + expireTime
	data = Base64Encode(data)
	dataSign := Md5(Md5(data) + secret)
	return dataSign
}

// AuthorMd5Encode md5 密钥签名
func AuthorMd5Encode(author *CrossAppAuthor, secret string, expire time.Duration) (string, error) {
	expireTime := "-1"
	if expire >= 0 {
		t := time.Now().Add(expire * time.Second)
		expireTime = strconv.Itoa(int(t.Unix()))
	}
	dataSign := createAuthorDataSign(author, secret, expireTime)
	return Base64Encode(expireTime + "." + dataSign), nil
}

// AuthorMd5Decode 验证数据签名
func AuthorMd5Decode(ak, userId string, nickname string, avatarUrl, accessKey string, secret string) error {
	str, err := Base64Decode(accessKey)
	log.Printf("decode accessKey res:%s, err:%v", str, err)
	if err != nil {
		return errors.New("数据签名无效")
	}
	items := strings.Split(str, ".")
	if len(items) != 2 {
		return errors.New("数据签名无效")
	}
	unixTime, err := strconv.Atoi(items[0])
	if err != nil {
		return errors.New("数据签名无效")
	}
	// expire 为-1时不过期
	if unixTime >= 0 {
		t := time.Unix(int64(unixTime), 0)
		if time.Now().After(t) {
			return errors.New("签名已过期")
		}
	}
	author := &CrossAppAuthor{
		AK: ak,
		SimpleBaseAuthor: SimpleBaseAuthor{
			UserId:   userId,
			NickName: nickname,
		},
		AvatarUrl: avatarUrl,
	}
	fmt.Printf("verify ak=%s,userId=%s,nickname=%s,avatarUrl=%s,expire=%d, secret=%s\n", ak, userId, nickname, avatarUrl, unixTime, secret)
	curSign := createAuthorDataSign(author, secret, strconv.Itoa(unixTime))
	fmt.Printf("verify cur sign=%s, test sign=%s\n", curSign, items[1])
	if curSign != items[1] {
		return errors.New("数据签名无效")
	}
	return nil
}
