package utils

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
	"log"
	"time"
)

type SimpleTokenPayload struct {
	Payload interface{} `json:"payload"`
	jwt.StandardClaims
}

type SimpleJwtOptions struct {
	TokenJwtExpSec time.Duration
	TokenJwtAlg    string
	TokenJwtSecret string
}

var DefaultJwtOptions = SimpleJwtOptions{
	TokenJwtExpSec: 60 * 60 * 30,
	TokenJwtAlg:    "HS256",
	TokenJwtSecret: "123456789",
}

type SimpleJwtToken struct {
}

var SimJwtToken = &SimpleJwtToken{}

// Encode 生成token
func (jt *SimpleJwtToken) Encode(payload interface{}, target *JwtTarget, opts *SimpleJwtOptions) (string, error) {
	var options = DefaultJwtOptions
	if opts != nil {
		options.TokenJwtExpSec = opts.TokenJwtExpSec
		options.TokenJwtSecret = opts.TokenJwtSecret
		options.TokenJwtAlg = opts.TokenJwtAlg
	}
	var alg jwt.SigningMethod
	if al, ok := algMaps[options.TokenJwtAlg]; ok {
		alg = al
	} else {
		alg = DefaultJwtAlg
	}
	now := time.Now()
	expireTime := now.Add(options.TokenJwtExpSec * time.Second)
	claims := SimpleTokenPayload{
		Payload: payload,
		StandardClaims: jwt.StandardClaims{
			//Audience:  "",                // 标识token的接收者
			ExpiresAt: expireTime.Unix(), // 过期时间.通常与Unix UTC时间做对比过期后token无效
			//Id:        "",                // 自定义的id号
			IssuedAt: now.Unix(), // 签名发行时间
			//Issuer:    "",                // 签名的发行者
			//NotBefore: 0,                 // 这条token信息生效时间.这个值可以不设置,但是设定后,一定要大于当前Unix UTC,否则token将会延迟生效.
			//Subject: "", // 签名面向的用户
		},
	}
	if target != nil {
		claims.StandardClaims.Issuer = target.Issuer
		claims.StandardClaims.Subject = target.Subject
	}

	token := jwt.NewWithClaims(alg, claims)
	secret := options.TokenJwtSecret
	tokenString, err := token.SignedString([]byte(secret))
	if err != nil {
		log.Printf("SignedString error:%s\n", err)
		return "", err
	}
	return tokenString, nil
}

// Decode 验证token
func (jt *SimpleJwtToken) Decode(tokenStr string, opts *SimpleJwtOptions) (interface{}, *JwtTarget, error) {
	var options = DefaultJwtOptions
	if opts != nil {
		options.TokenJwtExpSec = opts.TokenJwtExpSec
		options.TokenJwtSecret = opts.TokenJwtSecret
		options.TokenJwtAlg = opts.TokenJwtAlg
	}
	claims := &SimpleTokenPayload{
		Payload:        "",
		StandardClaims: jwt.StandardClaims{},
	}
	token, err := jwt.ParseWithClaims(tokenStr, claims, func(token *jwt.Token) (i interface{}, err error) {
		return []byte(options.TokenJwtSecret), nil
	})
	if err != nil {
		log.Printf("decode token error:%s\n", err)
		return "", nil, err
	}
	if !token.Valid {
		return "", nil, errors.New("令牌无效")
	}
	return claims.Payload, &JwtTarget{
		Issuer:  claims.Issuer,
		Subject: claims.Subject,
	}, nil
}
