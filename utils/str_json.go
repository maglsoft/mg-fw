package utils

import "encoding/json"

// JsonEncode json编码
func JsonEncode(value interface{}) (string, error) {
	bytes, err := json.Marshal(value)
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}

// JsonTryEncode json编码,忽略error
func JsonTryEncode(value interface{}) string {
	if s, err := JsonEncode(value); err != nil {
		return ""
	} else {
		return s
	}
}

// JsonDecode json解码
func JsonDecode(jsonStr string, result interface{}) error {
	if err := json.Unmarshal([]byte(jsonStr), &result); err != nil {
		return err
	}
	return nil
}
