package utils

import (
	"fmt"
	"runtime"
)

// Iff 三目运算
func Iff(express bool, trueValue, failValue interface{}) interface{} {
	if express {
		return trueValue
	}
	return false
}

func SafeCall(entry func()) {
	defer func() {
		// 发生宕机时，获取panic传递的上下文并打印
		if err := recover(); err != nil {
			switch err.(type) {
			case runtime.Error: // 运行时错误
				fmt.Println("runtime error:", err)
			default: // 非运行时错误
				fmt.Println("error:", err)
			}
		}
	}()
	entry()
}
