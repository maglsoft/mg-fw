package utils

import "encoding/base64"

// Base64Encode base64编码
func Base64Encode(str string) string {
	return base64.StdEncoding.EncodeToString([]byte(str))
}

// Base64Decode base64解码
func Base64Decode(base64Str string) (string, error) {
	if base64Str == "" {
		return "", nil
	}
	if base64Str[len(base64Str)-1] == '=' {
		bytes, err := base64.StdEncoding.DecodeString(base64Str)
		if err != nil {
			return base64Str, err
		}
		return string(bytes), nil
	}
	bytes, err := base64.RawStdEncoding.DecodeString(base64Str)
	if err != nil {
		return base64Str, err
	}
	return string(bytes), err
}
