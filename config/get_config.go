package config

import (
	"gitee.com/maglsoft/mg-fw/os/gfile"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"os"
	"path"
	"sync"
)

var _viper *viper.Viper
var EnvDev = false
var once sync.Once

// GetConfig 获取应用配置
func GetConfig(configName ...string) *viper.Viper {
	once.Do(func() {
		cfgName := "appSetting"
		if len(configName) > 0 && configName[0] != "" {
			cfgName = configName[0]
		}
		workDir, err := os.Getwd()
		if err != nil {
			panic("读取执行目录出错" + err.Error())
		}
		pkgMainPath := gfile.MainPkgPath()
		pwdDir, _ := os.Getwd()

		_viper = viper.New()
		pflag.StringP("APP_ENV", "e", "", "--APP_ENV=dev|production 运行环境")

		_viper.BindPFlags(pflag.CommandLine)
		pflag.Parse()

		var goEnv = _viper.GetString("APP_ENV")
		var configFile = cfgName
		EnvDev = goEnv == "dev"
		if EnvDev {
			configFile = cfgName + ".dev"
		}
		pwdConfDir := path.Join(pwdDir, "conf")

		_viper.SetConfigName(configFile)
		_viper.SetConfigType("yml")

		_viper.AddConfigPath(pwdConfDir)
		_viper.AddConfigPath(pwdDir)
		_viper.AddConfigPath(workDir)
		_viper.AddConfigPath("conf")
		_viper.AddConfigPath(pkgMainPath)
		err = _viper.ReadInConfig()
		if err != nil {
			panic(err)
		}
		if EnvDev {
			_viper.Debug()
		}
	})
	return _viper
}
