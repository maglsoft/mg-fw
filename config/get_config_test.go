package config

import (
	"reflect"
	"testing"
)

func TestGetConfig(t *testing.T) {
	_viper := GetConfig()
	apps := _viper.Get("apps")
	t.Logf("apps:%v, type:%s", apps, reflect.TypeOf(apps))
	if appList, ok := apps.([]any); !ok {
		t.Fatalf("配置apps节点不是否有的数组列表类型")
	} else {
		t.Logf("appList:%v", appList)
	}
}
