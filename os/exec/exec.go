package exec

import (
	"github.com/pkg/errors"
	"golang.org/x/net/context"
	"io"
	"log"
	"os/exec"
	"strings"
	"sync/atomic"
)

type ShellCommand interface {
	IsRun() bool
	Cancel()
	Input(param string)
	Stderr() string
	Stdout() string
	SetDir(dir string)
}

type Command struct {
	cmd       *exec.Cmd
	dir       string
	ctx       context.Context
	cancel    context.CancelFunc
	outResult strings.Builder
	errResult strings.Builder
	input     io.WriteCloser
	runState  int32
}

func (c *Command) IsRun() bool {
	if n := atomic.LoadInt32(&c.runState); n > 0 {
		return true
	}
	return false
}

func (c *Command) Cancel() {
	if c.IsRun() && c.cancel != nil {
		c.cancel()
		c.cancel = nil
	}
}

func (c *Command) Stderr() string {
	return c.errResult.String()
}

func (c *Command) Stdout() string {
	return c.outResult.String()
}

func (c *Command) SetDir(dir string) {
	c.dir = dir
}

func (c *Command) Input(param string) {
	if c.input != nil {
		log.Printf("录制 输入:%s\n", param)
		c.input.Write([]byte(param))
	}
}

func (c *Command) Run(name string, args ...string) error {
	rootCtx := context.Background()
	c.errResult = strings.Builder{}
	c.outResult = strings.Builder{}
	c.ctx, c.cancel = context.WithCancel(rootCtx)
	cmd := exec.CommandContext(c.ctx, name, args...)
	cmd.Dir = c.dir
	outReader, err := cmd.StdoutPipe()
	if err != nil {
		return err
	}
	defer outReader.Close()
	errorReader, err := cmd.StderrPipe()
	if err != nil {
		return err
	}
	defer errorReader.Close()
	c.input, err = cmd.StdinPipe()
	if err != nil {
		return err
	}
	defer func() {
		c.input.Close()
		c.input = nil
	}()

	atomic.StoreInt32(&c.runState, 1)
	defer func() {
		atomic.StoreInt32(&c.runState, 0)
	}()
	if err = cmd.Start(); err != nil {
		return err
	}
	errRecvBuf := make([]byte, 1024)
	go func() {
		for {
			n, err := errorReader.Read(errRecvBuf)
			if err != nil {
				if errors.Is(err, io.EOF) {
					return
				}
			}
			if n > 0 {
				if c.errResult.Len() >= 2048 {
					c.errResult.Reset()
					c.errResult.Grow(2048)
				}
				c.errResult.Write(errRecvBuf[:n])
			}
		}
	}()
	outRecvBuff := make([]byte, 512)
	for {
		n, err := outReader.Read(outRecvBuff)
		if err != nil {
			if errors.Is(err, io.EOF) {
				break
			}
		}
		if n > 0 {
			if c.outResult.Len() >= 512 {
				c.outResult.Reset()
				c.outResult.Grow(512)
			}
			c.outResult.Write(outRecvBuff[:n])
		}
	}
	if err := cmd.Wait(); err != nil {
		return err
	}
	if cmd.ProcessState.Success() {
		return nil
	} else {
		return errors.Errorf("exit code:%d", cmd.ProcessState.ExitCode())
	}
}
