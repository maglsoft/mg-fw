package exec

import (
	"testing"
	"time"
)

func TestCommand(t *testing.T) {
	cmd := &Command{}
	go func() {
		select {
		case <-time.After(10 * time.Second):
			t.Logf("5秒准备结束\n")
			cmd.Input("q")
		}
	}()
	// "-movflags", "faststart"
	err := cmd.Run("ffmpeg", "-y", "-re", "-i", "rtmp://live.freetable.cn/live/632142f5864a066e3bbfb675", "-vcodec", "libx264", "-acodec", "libfdk_aac", "-pix_fmt", "yuv420p", "-f", "mp4", "-movflags", "faststart", "./test.mp4")
	t.Logf("5秒结束\n")
	if err != nil {
		t.Fatalf("err:%s, msg:%s\n", err, cmd.Stderr())
	}
	t.Logf("result:%s, stderr:%s\n", cmd.Stdout(), cmd.Stderr())
}
