package glogger

import (
	"fmt"
)

type ColorPrintMode int

const (
	// ColorPrintModeDefault 终端默认设置
	ColorPrintModeDefault ColorPrintMode = 0
	// ColorPrintModeHighlight 高亮显示
	ColorPrintModeHighlight ColorPrintMode = 1
	// ColorPrintModeUnderline 使用下划线
	ColorPrintModeUnderline ColorPrintMode = 4
	// ColorPrintModeFlash 闪烁
	ColorPrintModeFlash ColorPrintMode = 5
	// ColorPrintModeReverse 反白
	ColorPrintModeReverse ColorPrintMode = 7
	// ColorPrintModeHidden 不可见
	ColorPrintModeHidden ColorPrintMode = 8
)

type PrintColor int

const (
	// PrintTextColorBlack 黑色
	PrintTextColorBlack PrintColor = 30
	// PrintTextColorRed 红色
	PrintTextColorRed PrintColor = 31
	// PrintTextColorGreen 绿色
	PrintTextColorGreen PrintColor = 32
	// PrintTextColorYellow 黄色
	PrintTextColorYellow PrintColor = 33
	// PrintTextColorBlue 蓝色
	PrintTextColorBlue PrintColor = 34
	// PrintTextColorPurplishRed 紫红色
	PrintTextColorPurplishRed PrintColor = 35
	// PrintTextColorTurquoise 青蓝色
	PrintTextColorTurquoise PrintColor = 36
	// PrintTextColorWhite 白色
	PrintTextColorWhite PrintColor = 37

	// PrintBackgroundBlack 色黑
	PrintBackgroundBlack PrintColor = 40
	// PrintBackgroundRed 红色
	PrintBackgroundRed PrintColor = 41
	// PrintBackgroundGreen 绿色
	PrintBackgroundGreen PrintColor = 42
	// PrintBackgroundYellow 黄色
	PrintBackgroundYellow PrintColor = 43
	// PrintBackgroundBlue 蓝色
	PrintBackgroundBlue PrintColor = 44
	// PrintBackgroundPurplishRed 紫红色
	PrintBackgroundPurplishRed PrintColor = 35
	// PrintBackgroundTurquoise 青蓝色
	PrintBackgroundTurquoise PrintColor = 46
	// PrintBackgroundWhite 白色
	PrintBackgroundWhite PrintColor = 47
)

func ColorPrint(str string, mode ColorPrintMode, bc PrintColor, tc PrintColor) {
	// 其中0x1B是标记，[开始定义颜色，依次为：模式，背景色，前景色，0代表恢复默认颜色。
	fmt.Print(fmt.Sprintf("%c[%d;%d;%dm%s%c[0m", 0x1B, mode, bc, tc, str, 0x1B))
}
