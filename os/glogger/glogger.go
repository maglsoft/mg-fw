package glogger

import (
	"sync"
	"time"
)

type LogLevel string

const (
	LogLevelAll   LogLevel = "all"
	LogLevelInfo  LogLevel = "info"
	LogLevelDebug LogLevel = "debug"
	LogLevelWarn  LogLevel = "warn"
	LogLevelError LogLevel = "error"
)

type GLog struct {
	Level LogLevel
	Tag   string
	Date  time.Time
	Log   any
}

type GLogWriter interface {
	Write(log *GLog)
}

type GLogWriterGetter func() GLogWriter

type GLogger interface {
	Log(level LogLevel, tag string, data any)
	Info(tag string, data any)
	Debug(tag string, data any)
	Warn(tag string, data any)
	Error(tag string, data any)
}

type GLogOptions struct {
	Prefix string   `json:"prefix"`
	Level  LogLevel `json:"level"`
}

type baseLogger struct {
	options      *GLogOptions
	lock         sync.RWMutex
	writerGetter GLogWriterGetter
}

func NewBaseLogger(writer GLogWriterGetter, options *GLogOptions) *baseLogger {
	return &baseLogger{
		lock:         sync.RWMutex{},
		options:      options,
		writerGetter: writer,
	}
}

func (l *baseLogger) Log(level LogLevel, tag string, data any) {
	l.lock.Lock()
	defer l.lock.Unlock()
	if !((l.options.Level == "" || l.options.Level == LogLevelAll) || (l.options.Level == level)) {
		return
	}
	writer := l.writerGetter()
	if writer == nil {
		return
	}
	writer.Write(&GLog{
		Level: level,
		Tag:   tag,
		Date:  time.Now(),
		Log:   data,
	})
}

func (l *baseLogger) Info(tag string, data any) {
	l.Log(LogLevelInfo, tag, data)
}

func (l *baseLogger) Debug(tag string, data any) {
	l.Log(LogLevelDebug, tag, data)
}

func (l *baseLogger) Warn(tag string, data any) {
	l.Log(LogLevelWarn, tag, data)
}

func (l *baseLogger) Error(tag string, data any) {
	l.Log(LogLevelError, tag, data)
}
