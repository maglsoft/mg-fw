package glogger

import (
	"testing"
)

func TestNewConsoleLogger(t *testing.T) {
	clogger := NewConsoleLogger(&GLogOptions{
		Prefix: "test",
		Level:  LogLevelAll,
	})
	clogger.Log(LogLevelInfo, "test", "这是info的日志")
	clogger.Log(LogLevelWarn, "test", "这是warn的日志")
	clogger.Log(LogLevelDebug, "test", "这是debug的日志")
	clogger.Log(LogLevelError, "test", "这是error的日志")
}
