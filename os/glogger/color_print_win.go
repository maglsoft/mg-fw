//go:build windows

package glogger

//
//import (
//	"fmt"
//)
//
//type ColorPrintMode int
//
//const (
//	// ColorPrintModeDefault 终端默认设置
//	ColorPrintModeDefault ColorPrintMode = 0
//	// ColorPrintModeHighlight 高亮显示
//	ColorPrintModeHighlight ColorPrintMode = 1
//	// ColorPrintModeUnderline 使用下划线
//	ColorPrintModeUnderline ColorPrintMode = 4
//	// ColorPrintModeFlash 闪烁
//	ColorPrintModeFlash ColorPrintMode = 5
//	// ColorPrintModeReverse 反白
//	ColorPrintModeReverse ColorPrintMode = 7
//	// ColorPrintModeHidden 不可见
//	ColorPrintModeHidden ColorPrintMode = 8
//)
//
//type PrintColor int
//
//const (
//	// PrintTextColorBlack 黑色
//	PrintTextColorBlack PrintColor = 0
//	// PrintTextColorRed 红色
//	PrintTextColorRed PrintColor = 4
//	// PrintTextColorGreen 绿色
//	PrintTextColorGreen PrintColor = 2
//	// PrintTextColorYellow 黄色
//	PrintTextColorYellow PrintColor = 6
//	// PrintTextColorBlue 蓝色
//	PrintTextColorBlue PrintColor = 1
//	// PrintTextColorPurplishRed 紫红色
//	PrintTextColorPurplishRed PrintColor = 0xD
//	// PrintTextColorTurquoise 青蓝色
//	PrintTextColorTurquoise PrintColor = 5
//	// PrintTextColorWhite 白色
//	PrintTextColorWhite PrintColor = 7
//
//	// PrintBackgroundBlack 色黑
//	PrintBackgroundBlack PrintColor = 0
//	// PrintBackgroundRed 红色
//	PrintBackgroundRed PrintColor = 4
//	// PrintBackgroundGreen 绿色
//	PrintBackgroundGreen PrintColor = 2
//	// PrintBackgroundYellow 黄色
//	PrintBackgroundYellow PrintColor = 6
//	// PrintBackgroundBlue 蓝色
//	PrintBackgroundBlue PrintColor = 1
//	// PrintBackgroundPurplishRed 紫红色
//	PrintBackgroundPurplishRed PrintColor = 0xD
//	// PrintBackgroundTurquoise 青蓝色
//	PrintBackgroundTurquoise PrintColor = 5
//	// PrintBackgroundWhite 白色
//	PrintBackgroundWhite PrintColor = 7
//)
//
//func ColorPrint(str any, mode ColorPrintMode, bc PrintColor, tc PrintColor) {
//	color := fmt.Sprintf("%d%d", bc, tc)
//	proc := kernel32.NewProc("SetConsoleTextAttribute")
//	handle, _, _ := proc.Call(uintptr(syscall.Stdout), color)
//	fmt.Println(s)
//	handle, _, _ = proc.Call(uintptr(syscall.Stdout), color)
//	CloseHandle := kernel32.NewProc("CloseHandle")
//	CloseHandle.Call(handle)
//}
