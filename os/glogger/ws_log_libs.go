package glogger

import (
	"encoding/json"
	"github.com/gorilla/websocket"
	"log"
	"time"
)

func wsPackageMsg(event string, data any) ([]byte, error) {
	pk := &WSEventDataPk{
		Event: event,
		Data:  data,
	}
	bytes, err := json.Marshal(pk)
	if err != nil {
		return []byte{}, err
	}
	return bytes, nil
}

func wsLogEmitEvent(conn *websocket.Conn, event string, data any) {
	msg, err := wsPackageMsg(event, data)
	if err != nil {
		log.Printf("EmitEvent error:%s\n", err)
	}
	if err := conn.WriteMessage(websocket.TextMessage, msg); err != nil {
		log.Printf("send on_connect_ack error:%s\n", err)
	}
}

func wsLogEmitEventAndClose(conn *websocket.Conn, event string, data any) {
	wsLogEmitEvent(conn, event, data)
	time.AfterFunc(3*time.Second, func() {
		if err := conn.Close(); err != nil {
			log.Printf("websocket close client error:%s\n", err)
		}
	})
}
