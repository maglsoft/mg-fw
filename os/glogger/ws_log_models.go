package glogger

type WSEventDataPk struct {
	Event string `json:"event"`
	Data  any    `json:"data"`
}

type WSEventData struct {
	Code    int    `json:"code"`
	Msg     string `json:"msg"`
	Content any    `json:"content,omitempty"`
}

func NewEventError(msg string) *WSEventData {
	return &WSEventData{
		Code:    1,
		Msg:     msg,
		Content: nil,
	}
}

func NewWSEventData(data any) *WSEventData {
	return &WSEventData{
		Code:    0,
		Msg:     "",
		Content: data,
	}
}

type wsLogEventData struct {
	Level LogLevel `json:"level"`
	Tag   string   `json:"tag"`
	Log   any      `json:"log"`
}
