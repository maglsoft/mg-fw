package glogger

import (
	"context"
	"gitee.com/maglsoft/mg-fw/utils"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"sync"
	"time"
)

type GWSLogger interface {
	http.Handler
	GLogger
}

type wsLogSvr struct {
	upGrader   *websocket.Upgrader
	jwt        utils.JwtToken
	writerLock sync.RWMutex
	writers    []GWSLogWriter
	buffer     chan *wsLogEventData
	ctx        context.Context
	close      chan byte
}

func NewGWSLogger(ctx context.Context, jwt utils.JwtToken) GWSLogger {
	result := &wsLogSvr{
		upGrader: &websocket.Upgrader{
			HandshakeTimeout: 10 * time.Second,
			ReadBufferSize:   1024,
			WriteBufferSize:  1024,
			CheckOrigin: func(r *http.Request) bool {
				return true
			},
		},
		jwt:        jwt,
		writerLock: sync.RWMutex{},
		writers:    make([]GWSLogWriter, 0, 10),
		buffer:     make(chan *wsLogEventData, 256),
		ctx:        ctx,
		close:      make(chan byte),
	}
	go result.run()
	return result
}

func (ls *wsLogSvr) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	conn, err := ls.upGrader.Upgrade(writer, request, nil)
	if err != nil {
		log.Printf("error info %+v", err)
		return
	}
	params := request.URL.Query()
	token := params.Get("token")
	if token == "" {
		wsLogEmitEventAndClose(conn, wsLogEventConnectAck, NewEventError("缺少token参数"))
		return
	}
	author, _, err := ls.jwt.DecodeJwtToken(token, &utils.SimpleBaseAuthor{})
	if err != nil {
		wsLogEmitEventAndClose(conn, wsLogEventConnectAck, NewEventError(err.Error()))
	}
	ls.accept(conn, author)
}

func (ls *wsLogSvr) accept(conn *websocket.Conn, author utils.BaseAuthor) {
	ls.writerLock.Lock()
	defer ls.writerLock.Unlock()
	writer := newLogWriter(ls.ctx, conn)
	ls.writers = append(ls.writers, writer)
	go writer.writeLoop()
	go func() {
		writer.readLoop()
		// 断开连接
		ls.writerLock.Lock()
		defer ls.writerLock.Unlock()
		i := 0
		for _, w := range ls.writers {
			if w.ID() != writer.ID() {
				ls.writers[i] = w
				i++
			}
		}
		ls.writers = ls.writers[:i]
	}()
	wsLogEmitEvent(conn, wsLogEventConnectAck, &WSEventData{
		Code: 0,
		Msg:  "连接成功",
		Content: map[string]any{
			"userId":   author.GetUserId(),
			"nickname": author.GetNickname(),
		},
	})
}

func (ls *wsLogSvr) run() {
	go func() {
		<-ls.ctx.Done()
		close(ls.close)
	}()
	for {
		select {
		case <-ls.close:
			return
		case item, ok := <-ls.buffer:
			if !ok {
				return
			}
			ls.flushLog(item)
		}
	}
}

func (ls *wsLogSvr) flushLog(item *wsLogEventData) {
	ls.writerLock.RLock()
	defer ls.writerLock.RUnlock()
	for _, w := range ls.writers {
		w.Log(item.Level, item.Tag, item.Log)
	}
}

func (ls *wsLogSvr) Log(level LogLevel, tag string, data any) {
	ls.buffer <- &wsLogEventData{
		Level: level,
		Tag:   tag,
		Log:   data,
	}
}

func (ls *wsLogSvr) Info(tag string, data any) {
	ls.Log(LogLevelInfo, tag, data)
}

func (ls *wsLogSvr) Debug(tag string, data any) {
	ls.Log(LogLevelDebug, tag, data)
}

func (ls *wsLogSvr) Warn(tag string, data any) {
	ls.Log(LogLevelWarn, tag, data)
}

func (ls *wsLogSvr) Error(tag string, data any) {
	ls.Log(LogLevelError, tag, data)
}
