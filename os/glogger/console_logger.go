package glogger

import (
	"gitee.com/maglsoft/mg-fw/db/dbtype/unixTime"
	"github.com/spf13/cast"
)

type consoleWriter struct{}

func (c *consoleWriter) Write(log *GLog) {
	if log == nil {
		return
	}
	bc := PrintBackgroundBlack
	tc := PrintTextColorWhite
	switch log.Level {
	case LogLevelDebug:
		tc = PrintTextColorBlue
		break
	case LogLevelWarn:
		tc = PrintTextColorYellow
		break
	case LogLevelError:
		tc = PrintTextColorRed
		break
	}
	logHeader := "[" + string(log.Level) + "]" + log.Date.Format(unixTime.DateTimeFormatter) + "(" + log.Tag + "):\n"
	ColorPrint(logHeader, ColorPrintModeDefault, bc, tc)
	s := cast.ToString(log.Log)
	ColorPrint(s, ColorPrintModeDefault, PrintBackgroundBlack, PrintTextColorWhite)
	ColorPrint("\n", ColorPrintModeDefault, PrintBackgroundBlack, PrintTextColorWhite)
}

var _ GLogWriter = (*consoleWriter)(nil)

type consoleLogger struct {
	*baseLogger
	writer *consoleWriter
}

var _ GLogger = (*consoleLogger)(nil)

func NewConsoleLogger(options *GLogOptions) GLogger {
	writer := &consoleWriter{}
	return &consoleLogger{
		writer: writer,
		baseLogger: NewBaseLogger(func() GLogWriter {
			return writer
		}, options),
	}
}
