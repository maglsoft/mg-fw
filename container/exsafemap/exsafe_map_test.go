package exsafemap

import (
	"math/rand"
	"strconv"
	"sync"
	"testing"
	"time"
)

func TestExSafeMap(t *testing.T) {
	var w sync.WaitGroup
	now := time.Now()
	esmap := New(1024)
	esmap.Run()
	for i := 0; i < 1000; i++ {
		w.Add(1)
		go func(n int) {
			defer w.Done()
			for j := 0; j < 1000; j++ {
				esmap.Set(strconv.Itoa(n)+"_"+strconv.Itoa(j), n*j, 1*time.Second)
			}
		}(i)
	}
	w.Wait()
	for i := 0; i < 100; i++ {
		w.Add(1)
		go func() {
			defer w.Done()
			n := rand.Intn(1000)
			n2 := rand.Intn(1000)
			key := strconv.Itoa(n) + "_" + strconv.Itoa(n2)
			v, ok := esmap.Get(key)
			t.Logf("get key=%s,value=%v,ok=%v\n", key, v, ok)
		}()
	}
	w.Wait()
	keys := esmap.Keys("10_*")
	t.Logf("keys[10_*]:%v\n", keys)
	sec := time.Since(now).Milliseconds()
	t.Logf("sec:%d\n", sec)
	t.Logf("map size:%d\n", esmap.Size())
	ticker := time.NewTicker(40 * time.Second)
	<-ticker.C
	now = time.Now()
	esmap.Close()
	//esmap.ClearExpire()
	sec = time.Since(now).Milliseconds()
	t.Logf("esmap.close use time:%v\n", sec)
	t.Logf("after clear expire map size:%d\n", esmap.Size())
}
