package exsafemap

import (
	"fmt"
	"testing"
	"time"
)

func BKDRHash(str string) uint64 {
	seed := uint64(131) // 31 131 1313 13131 131313 etc..
	hash := uint64(0)
	for i := 0; i < len(str); i++ {
		hash = (hash * seed) + uint64(str[i])
	}
	return hash & 0x7FFFFFFF
}

const bucketSize = 256

func TestBKDRHash(t *testing.T) {
	start := time.Now()
	keyCounts := make(map[string]int)
	for i := 0; i < 1000000; i++ {
		hash := BKDRHash(fmt.Sprintf("hello,word:%d-%d", i, time.Now().UnixMilli()))
		index := hash % bucketSize
		key := fmt.Sprintf("index_%d", index)
		if n, ok := keyCounts[key]; ok {
			keyCounts[key] = n + 1
		} else {
			keyCounts[key] = 1
		}
	}
	sec := time.Now().Sub(start).Milliseconds()
	t.Logf("%v\n", keyCounts)
	t.Logf("time:%d\n", sec)
}
