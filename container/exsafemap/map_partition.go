package exsafemap

import (
	"path/filepath"
	"sync"
	"time"
)

type mapPartition struct {
	values map[string]*mapKeypair
	lock   sync.RWMutex
}

func newMapPartition() *mapPartition {
	return &mapPartition{
		values: make(map[string]*mapKeypair),
		lock:   sync.RWMutex{},
	}
}

// get 获取值
func (mp *mapPartition) get(key string) (any, bool) {
	mp.lock.RLock()
	defer mp.lock.RUnlock()
	v, ok := mp.values[key]
	if !ok {
		return nil, false
	}
	if (v.expireTime == -1) || (v.expireTime > 0 && v.expireTime < time.Now().Unix()) {
		delete(mp.values, key)
		return nil, false
	}
	return v.value, true
}

// set 设置值
func (mp *mapPartition) set(key string, val any, ex ...time.Duration) {
	mp.lock.Lock()
	defer mp.lock.Unlock()
	item := &mapKeypair{
		value:      val,
		expireTime: 0,
	}
	if len(ex) > 0 && ex[0] > 0 {
		item.expireTime = time.Now().Add(ex[0]).Unix()
	}
	mp.values[key] = item
}

// forEach 遍历
func (mp *mapPartition) forEach(each func(k string, v any) bool) {
	for k, v := range mp.values {
		if !(each(k, v)) {
			return
		}
	}
}

// clear 清空
func (mp *mapPartition) clear() {
	mp.lock.Lock()
	defer mp.lock.Unlock()
	mp.values = make(map[string]*mapKeypair)
}

func (mp *mapPartition) size() int {
	return len(mp.values)
}

func (mp *mapPartition) keys(pattern string) []string {
	result := make([]string, 0, len(mp.values))
	for k := range mp.values {
		if b, err := filepath.Match(pattern, k); err == nil && b {
			result = append(result, k)
		}
	}
	return result
}

// clearExpire 清理过期键
func (mp *mapPartition) clearExpire(expire time.Time) {
	mp.lock.Lock()
	defer mp.lock.Unlock()
	deleteKeys := make([]string, 0, len(mp.values))
	expireUnix := expire.Unix()
	for k, v := range mp.values {
		if v.expireTime > 0 && v.expireTime < expireUnix {
			deleteKeys = append(deleteKeys, k)
			go func() {
				mp.lock.Lock()
				defer mp.lock.Unlock()
				v.expireTime = -1
			}()
		}
	}
	if len(deleteKeys) == 0 {
		return
	}
	for _, k := range deleteKeys {
		delete(mp.values, k)
	}
}
