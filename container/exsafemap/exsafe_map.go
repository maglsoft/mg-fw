package exsafemap

import (
	"sync"
	"time"
)

type EXSafeMap struct {
	options    *Options
	bucketSize int
	partitions []*mapPartition
	lock       sync.RWMutex
	close      chan byte
}

type Options struct {
	CheckExpireSpeed time.Duration
}

var defaultCheckExpireSpeed = 30 * time.Second

var defaultOptions = &Options{
	CheckExpireSpeed: defaultCheckExpireSpeed,
}

func New(bucketSize int, options ...*Options) *EXSafeMap {
	opts := defaultOptions
	if len(options) > 0 && options[0] != nil {
		opts = options[0]
	}
	result := &EXSafeMap{
		options:    opts,
		bucketSize: bucketSize,
		partitions: make([]*mapPartition, bucketSize, bucketSize),
		close:      make(chan byte),
	}
	for i := 0; i < bucketSize; i++ {
		result.partitions[i] = newMapPartition()
	}
	return result
}

func (m *EXSafeMap) bkdrHash(key string) uint64 {
	seed := uint64(131) // 31 131 1313 13131 131313 etc..
	hash := uint64(0)
	for i := 0; i < len(key); i++ {
		hash = (hash * seed) + uint64(key[i])
	}
	return hash & 0x7FFFFFFF
}

func (m *EXSafeMap) getPartition(key string) *mapPartition {
	hashcode := m.bkdrHash(key)
	bkIdx := int(hashcode) % m.bucketSize
	if m.partitions[bkIdx] == nil {
		m.partitions[bkIdx] = newMapPartition()
	}
	return m.partitions[bkIdx]
}

func (m *EXSafeMap) Get(key string) (any, bool) {
	par := m.getPartition(key)
	return par.get(key)
}

func (m *EXSafeMap) Set(key string, val any, ex ...time.Duration) {
	par := m.getPartition(key)
	par.set(key, val, ex...)
}

func (m *EXSafeMap) Clear() {
	for _, par := range m.partitions {
		par.clear()
	}
}

func (m *EXSafeMap) ForEach(each func(k string, v any) bool) {
	for _, par := range m.partitions {
		par.forEach(each)
	}
}

func (m *EXSafeMap) Size() int64 {
	var n int64 = 0
	for _, par := range m.partitions {
		n += int64(par.size())
	}
	return n
}

func (m *EXSafeMap) Keys(pattern string) []string {
	result := make([]string, 0, m.bucketSize)
	for _, p := range m.partitions {
		result = append(result, p.keys(pattern)...)
	}
	return result
}

func (m *EXSafeMap) ClearExpire() {
	now := time.Now()
	var w sync.WaitGroup
	clears := make(chan *mapPartition, 100)
	go func() {
		for {
			select {
			case p, ok := <-clears:
				if !ok {
					return
				}
				p.clearExpire(now)
				w.Done()
			}
		}
	}()
	for _, par := range m.partitions {
		w.Add(1)
		clears <- par
	}
	close(clears)
	w.Wait()
}

func (m *EXSafeMap) Run() {
	if m.options.CheckExpireSpeed == 0 {
		m.options.CheckExpireSpeed = defaultCheckExpireSpeed
	}
	ticker := time.NewTicker(m.options.CheckExpireSpeed)
	go func() {
		for {
			select {
			case <-m.close:
				return
			case <-ticker.C:
				m.ClearExpire()
			}
		}
	}()
}

func (m *EXSafeMap) Close() {
	select {
	case <-m.close:
		return
	default:
	}
	close(m.close)
}
