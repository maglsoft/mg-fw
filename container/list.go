package container

import (
	"log"
	"sync"
)

type ListElement interface {
	~int | ~int8 | ~int16 | ~int32 | ~int64 |
		~uint | ~uint8 | ~uint16 | ~uint32 | ~uint64 | ~uintptr |
		~float32 | ~float64 |
		~string
}

type List[T ListElement] struct {
	items  []T
	lock   sync.RWMutex
	unsafe bool
}

func NewList[T ListElement](unsafe ...bool) *List[T] {
	b := false
	if len(unsafe) > 0 {
		b = unsafe[0]
	}
	return &List[T]{
		items:  make([]T, 0, 128),
		lock:   sync.RWMutex{},
		unsafe: b,
	}
}

func (l *List[T]) Size() int {
	return len([]T(l.items))
}

func (l *List[T]) Add(item T) {
	if !l.unsafe {
		l.lock.Lock()
		defer l.lock.Unlock()
	}
	l.items = append([]T(l.items), item)
}

func (l *List[T]) Remove(item T) {
	if !l.unsafe {
		l.lock.Lock()
		defer l.lock.Unlock()
	}
	if len([]T(l.items)) > 0 {
		i := 0
		for _, c := range l.items {
			log.Printf("list remove item:%v\n", c)
			if c != item {
				l.items[i] = c
				i++
			}
		}
		l.items = l.items[:i]
		log.Printf("list remove item. items:%v\n", l.items)
	}
}

func (l *List[T]) IndexOf(item T) int {
	for i, c := range l.items {
		if c == item {
			return i
		}
	}
	return -1
}

func (l *List[T]) ItemOf(idx int) *T {
	if idx < 0 || idx > len([]T(l.items)) {
		return nil
	}
	return &l.items[idx]
}

func (l *List[T]) Clear() {
	if !l.unsafe {
		l.lock.Lock()
		defer l.lock.Unlock()
	}
	l.items = make([]T, 0, 128)
}

func (l *List[T]) Items() []T {
	if !l.unsafe {
		l.lock.RLock()
		defer l.lock.RUnlock()
	}
	return l.items
}
